-- --------------------------------------------------------
-- Host:                         192.168.33.10
-- Server version:               5.5.50-0ubuntu0.14.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for parkonline
-- CREATE DATABASE IF NOT EXISTS `parkonline` /*!40100 DEFAULT CHARACTER SET utf32 COLLATE utf32_unicode_ci */;
-- USE `parkonline`;


-- Dumping structure for table parkonline.estado
CREATE TABLE IF NOT EXISTS `estado` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) COLLATE utf32_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- Dumping data for table parkonline.estado: ~5 rows (approximately)
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` (`id`, `descricao`) VALUES
	(1, 'Pedido'),
	(2, 'Pendente'),
	(3, 'Aprovado'),
	(4, 'Rejeitado'),
	(5, 'Terminado');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;


-- Dumping structure for table parkonline.factura
CREATE TABLE IF NOT EXISTS `factura` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `data_emissao` date NOT NULL,
  `id_condutor` int(10) unsigned NOT NULL,
  `mes_faturacao` date NOT NULL,
  `Total` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mes_faturacao` (`mes_faturacao`),
  KEY `FK_factura_utilizador` (`id_condutor`),
  CONSTRAINT `FK_factura_utilizador` FOREIGN KEY (`id_condutor`) REFERENCES `utilizador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- Dumping data for table parkonline.factura: ~1 rows (approximately)
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` (`id`, `data_emissao`, `id_condutor`, `mes_faturacao`, `Total`) VALUES
	(1, '2017-05-01', 1, '2017-04-14', 0);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;


-- Dumping structure for table parkonline.parque
CREATE TABLE IF NOT EXISTS `parque` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) COLLATE utf32_unicode_ci NOT NULL,
  `morada` varchar(50) COLLATE utf32_unicode_ci NOT NULL,
  `localidade` varchar(50) COLLATE utf32_unicode_ci NOT NULL,
  `cod_postal` varchar(7) COLLATE utf32_unicode_ci NOT NULL,
  `preco_min` double NOT NULL,
  `gps` varchar(25) COLLATE utf32_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- Dumping data for table parkonline.parque: ~1 rows (approximately)
/*!40000 ALTER TABLE `parque` DISABLE KEYS */;
INSERT INTO `parque` (`id`, `nome`, `morada`, `localidade`, `cod_postal`, `preco_min`, `gps`) VALUES
	(1, 'Estacionamento Restauradores', 'Praça dos Restauradores', 'Lisboa', '1250-18', 0.9, '38.7166128,-9.1423065');
/*!40000 ALTER TABLE `parque` ENABLE KEYS */;


-- Dumping structure for view parkonline.parque2
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `parque2` (
	`id` INT(10) UNSIGNED NOT NULL,
	`nome` VARCHAR(50) NOT NULL COLLATE 'utf32_unicode_ci',
	`morada` VARCHAR(50) NOT NULL COLLATE 'utf32_unicode_ci',
	`localidade` VARCHAR(50) NOT NULL COLLATE 'utf32_unicode_ci',
	`cod_postal` VARCHAR(7) NOT NULL COLLATE 'utf32_unicode_ci',
	`preco_min` DOUBLE NOT NULL,
	`gps` VARCHAR(25) NOT NULL COLLATE 'utf32_unicode_ci',
	`total_lugares` DECIMAL(32,0) NULL
) ENGINE=MyISAM;


-- Dumping structure for table parkonline.parque_tipo_lugar
CREATE TABLE IF NOT EXISTS `parque_tipo_lugar` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_parque` int(10) unsigned NOT NULL,
  `id_tipo_lugar` int(10) unsigned NOT NULL,
  `total_lugares` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_parque_id_tipo_lugar` (`id_parque`,`id_tipo_lugar`),
  KEY `FK_parque_tipo_lugar_tipo_lugar` (`id_tipo_lugar`),
  CONSTRAINT `FK_parque_tipo_lugar_tipo_lugar` FOREIGN KEY (`id_tipo_lugar`) REFERENCES `tipo_lugar` (`id`),
  CONSTRAINT `FK__parque` FOREIGN KEY (`id_parque`) REFERENCES `parque` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- Dumping data for table parkonline.parque_tipo_lugar: ~3 rows (approximately)
/*!40000 ALTER TABLE `parque_tipo_lugar` DISABLE KEYS */;
INSERT INTO `parque_tipo_lugar` (`id`, `id_parque`, `id_tipo_lugar`, `total_lugares`) VALUES
	(1, 1, 1, 40),
	(2, 1, 2, 4),
	(3, 1, 3, 4);
/*!40000 ALTER TABLE `parque_tipo_lugar` ENABLE KEYS */;


-- Dumping structure for table parkonline.reserva
CREATE TABLE IF NOT EXISTS `reserva` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_veiculo` int(10) unsigned NOT NULL,
  `data_reserva` datetime NOT NULL COMMENT 'data que o condutor reservou para estacionar',
  `id_parque` int(10) unsigned NOT NULL,
  `id_tipo_lugar` int(10) unsigned NOT NULL,
  `hora_entrada` datetime DEFAULT NULL COMMENT 'valor fornecido quando estado de reserva passa para terminado',
  `hora_saida` datetime DEFAULT NULL COMMENT 'valor fornecido quando estado de reserva passa para terminado',
  `preco` float DEFAULT NULL COMMENT 'valor fornecido quando estado de reserva passa para terminado',
  PRIMARY KEY (`id`),
  KEY `id_parque` (`id_parque`,`id_tipo_lugar`),
  KEY `FK_reserva_veiculo` (`id_veiculo`),
  CONSTRAINT `FK_reserva_veiculo` FOREIGN KEY (`id_veiculo`) REFERENCES `veiculo` (`id`),
  CONSTRAINT `reserva_ibfk_1` FOREIGN KEY (`id_parque`, `id_tipo_lugar`) REFERENCES `parque_tipo_lugar` (`id_parque`, `id_tipo_lugar`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- Dumping data for table parkonline.reserva: ~2 rows (approximately)
/*!40000 ALTER TABLE `reserva` DISABLE KEYS */;
INSERT INTO `reserva` (`id`, `id_veiculo`, `data_reserva`, `id_parque`, `id_tipo_lugar`, `hora_entrada`, `hora_saida`, `preco`) VALUES
	(1, 1, '2017-04-23 08:00:00', 1, 1, '2017-04-23 08:00:00', '2017-04-23 12:00:00', 6),
	(2, 1, '2017-04-20 08:00:00', 1, 1, '2017-04-20 08:00:00', '2017-04-20 09:51:00', 1.7);
/*!40000 ALTER TABLE `reserva` ENABLE KEYS */;


-- Dumping structure for table parkonline.reserva_estado
CREATE TABLE IF NOT EXISTS `reserva_estado` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_reserva` bigint(20) unsigned NOT NULL,
  `id_estado` int(10) unsigned NOT NULL,
  `dia_hora` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_reserva_id_estado` (`id_reserva`,`id_estado`),
  KEY `FK_reserva_estado_estado` (`id_estado`),
  CONSTRAINT `FK_reserva_estado_estado` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id`),
  CONSTRAINT `FK_reserva_estado_reserva` FOREIGN KEY (`id_reserva`) REFERENCES `reserva` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- Dumping data for table parkonline.reserva_estado: ~8 rows (approximately)
/*!40000 ALTER TABLE `reserva_estado` DISABLE KEYS */;
INSERT INTO `reserva_estado` (`id`, `id_reserva`, `id_estado`, `dia_hora`) VALUES
	(1, 1, 1, '2017-04-19 12:30:00'),
	(2, 1, 2, '2017-04-19 12:30:02'),
	(3, 1, 3, '2017-04-19 12:35:00'),
	(5, 2, 1, '2017-04-20 12:00:00'),
	(6, 2, 2, '2017-04-20 12:00:02'),
	(7, 2, 3, '2017-04-20 12:02:00'),
	(8, 1, 5, '2017-04-29 10:00:00'),
	(9, 2, 5, '2017-04-24 12:52:57');
/*!40000 ALTER TABLE `reserva_estado` ENABLE KEYS */;


-- Dumping structure for table parkonline.tipo_lugar
CREATE TABLE IF NOT EXISTS `tipo_lugar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) COLLATE utf32_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- Dumping data for table parkonline.tipo_lugar: ~5 rows (approximately)
/*!40000 ALTER TABLE `tipo_lugar` DISABLE KEYS */;
INSERT INTO `tipo_lugar` (`id`, `descricao`) VALUES
	(1, 'Normal'),
	(2, 'Incapacitado'),
	(3, 'Gravida'),
	(4, 'Idoso'),
	(5, 'Eletrico');
/*!40000 ALTER TABLE `tipo_lugar` ENABLE KEYS */;


-- Dumping structure for table parkonline.utilizador
CREATE TABLE IF NOT EXISTS `utilizador` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `authenticationKey` varchar(100) DEFAULT NULL,
  `accessToken` varchar(100) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `tipoUtilizador` enum('Admin','Condutor','Parque') NOT NULL DEFAULT 'Condutor',
  `nif` varchar(9) NOT NULL,
  `apelido` varchar(50) NOT NULL,
  `morada` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table parkonline.utilizador: ~2 rows (approximately)
/*!40000 ALTER TABLE `utilizador` DISABLE KEYS */;
INSERT INTO `utilizador` (`id`, `nome`, `senha`, `authenticationKey`, `accessToken`, `email`, `telefone`, `tipoUtilizador`, `nif`, `apelido`, `morada`) VALUES
	(1, 'maria', 'qwe123', 'xxggzGN5WQI9rDloVtXfvsLLG3MuvZDo', '', 'mara@gggg.com', NULL, '', '', '', ''),
	(2, 'vitor', 'qwe123', 'ngggzGN5WQI9rDloVtXfvsLLG3MuvZDo', '', 'vitorcool@gmail.com', NULL, '', '', '', '');
/*!40000 ALTER TABLE `utilizador` ENABLE KEYS */;


-- Dumping structure for table parkonline.veiculo
CREATE TABLE IF NOT EXISTS `veiculo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_utilizador` int(11) unsigned NOT NULL,
  `matricula` varchar(20) COLLATE utf32_unicode_ci NOT NULL,
  `marca` varchar(20) COLLATE utf32_unicode_ci NOT NULL,
  `n_lugares` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_veiculo_utilizador` (`id_utilizador`),
  CONSTRAINT `FK_veiculo_utilizador` FOREIGN KEY (`id_utilizador`) REFERENCES `utilizador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- Dumping data for table parkonline.veiculo: ~1 rows (approximately)
/*!40000 ALTER TABLE `veiculo` DISABLE KEYS */;
INSERT INTO `veiculo` (`id`, `id_utilizador`, `matricula`, `marca`, `n_lugares`) VALUES
	(1, 2, '11-aa-22', 'peugeot', 5);
/*!40000 ALTER TABLE `veiculo` ENABLE KEYS */;



-- Dumping structure for view parkonline.parque2
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `parque2`;
CREATE  VIEW `parque2` AS select `parque`.`id` AS `id`,`parque`.`nome` AS `nome`,`parque`.`morada` AS `morada`,`parque`.`localidade` AS `localidade`,`parque`.`cod_postal` AS `cod_postal`,`parque`.`preco_min` AS `preco_min`,`parque`.`gps` AS `gps`,sum(`parque_tipo_lugar`.`total_lugares`) AS `total_lugares` from (`parque` join `parque_tipo_lugar` on((`parque_tipo_lugar`.`id_parque` = `parque`.`id`)));
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
