-- --------------------------------------------------------
-- Host:                         192.168.33.10
-- Server version:               5.5.50-0ubuntu0.14.04.1 - (Ubuntu)
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for parkonline
CREATE DATABASE IF NOT EXISTS `parkonline` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `parkonline`;


-- Dumping structure for table parkonline.estado
DROP TABLE IF EXISTS `estado`;
CREATE TABLE IF NOT EXISTS `estado` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table parkonline.estado: ~7 rows (approximately)
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` (`id`, `descricao`) VALUES
	(1, 'Pedido'),
	(2, 'Pendente'),
	(3, 'Aprovado'),
	(4, 'Rejeitado'),
	(5, 'Dentro'),
	(6, 'Terminado'),
	(7, 'Expirado');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;


-- Dumping structure for table parkonline.factura
DROP TABLE IF EXISTS `factura`;
CREATE TABLE IF NOT EXISTS `factura` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `data_emissao` date NOT NULL,
  `id_condutor` int(10) unsigned NOT NULL,
  `mes_faturacao` date NOT NULL,
  `Total` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mes_faturacao` (`mes_faturacao`),
  KEY `FK_factura_utilizador` (`id_condutor`),
  CONSTRAINT `FK_factura_utilizador` FOREIGN KEY (`id_condutor`) REFERENCES `utilizador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table parkonline.factura: ~4 rows (approximately)
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` (`id`, `data_emissao`, `id_condutor`, `mes_faturacao`, `Total`) VALUES
	(1, '2017-05-01', 1, '2017-04-14', 7.7),
	(2, '2017-04-30', 2, '2017-04-30', 7.58),
	(3, '2017-03-31', 2, '2017-03-31', 2),
	(4, '2017-02-28', 2, '2017-02-28', 10.5);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;


-- Dumping structure for table parkonline.parque
DROP TABLE IF EXISTS `parque`;
CREATE TABLE IF NOT EXISTS `parque` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `morada` varchar(50) NOT NULL,
  `localidade` varchar(50) NOT NULL,
  `cod_postal` varchar(8) NOT NULL,
  `preco_min` double NOT NULL,
  `gps` varchar(25) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table parkonline.parque: ~5 rows (approximately)
/*!40000 ALTER TABLE `parque` DISABLE KEYS */;
INSERT INTO `parque` (`id`, `nome`, `morada`, `localidade`, `cod_postal`, `preco_min`, `gps`, `lat`, `lng`) VALUES
	(1, 'Estacionamento Restauradores', 'Praça dos Restauradores', 'Lisboa', '1250-18', 0.9, '38.7166128,-9.1423065', 38.7166128, -9.1423065),
	(2, 'Estacionamento Mercado da Ribeira', 'Praça D.Luis I', 'Lisboa', '1200-148', 0.1, ',', 38.7070511, -9.216384),
	(3, 'Estacionamento Largo de Jesus', 'Tv. Convento de Jesus', 'Lisboa', '1200-231', 0.12, '', 38.712181, -9.220294),
	(4, 'Parque de Estacionamento da Basílica', 'Praça da Estrela 3', 'Lisboa', '1200-667', 0.22, '', 38.7126003, -9.2304795),
	(5, 'Praça de Camões', 'Praça Luis de Camões', 'Lisboa', '1200-243', 0.12, '', 38.7107341, -9.213348);
/*!40000 ALTER TABLE `parque` ENABLE KEYS */;


-- Dumping structure for table parkonline.parque_tipo_lugar
DROP TABLE IF EXISTS `parque_tipo_lugar`;
CREATE TABLE IF NOT EXISTS `parque_tipo_lugar` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_parque` int(10) unsigned NOT NULL,
  `id_tipo_lugar` int(10) unsigned NOT NULL,
  `total_lugares` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_parque_id_tipo_lugar` (`id_parque`,`id_tipo_lugar`),
  KEY `FK_parque_tipo_lugar_tipo_lugar` (`id_tipo_lugar`),
  CONSTRAINT `FK_parque_tipo_lugar_parque` FOREIGN KEY (`id_parque`) REFERENCES `parque` (`id`),
  CONSTRAINT `FK_parque_tipo_lugar_tipo_lugar` FOREIGN KEY (`id_tipo_lugar`) REFERENCES `tipo_lugar` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table parkonline.parque_tipo_lugar: ~6 rows (approximately)
/*!40000 ALTER TABLE `parque_tipo_lugar` DISABLE KEYS */;
INSERT INTO `parque_tipo_lugar` (`id`, `id_parque`, `id_tipo_lugar`, `total_lugares`) VALUES
	(1, 1, 1, 40),
	(2, 1, 2, 4),
	(3, 1, 3, 4),
	(4, 2, 1, 40),
	(5, 3, 3, 10),
	(6, 5, 4, 5);
/*!40000 ALTER TABLE `parque_tipo_lugar` ENABLE KEYS */;


-- Dumping structure for table parkonline.reserva
DROP TABLE IF EXISTS `reserva`;
CREATE TABLE IF NOT EXISTS `reserva` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_veiculo` int(10) unsigned NOT NULL,
  `data_reserva` datetime NOT NULL COMMENT 'data que o condutor reservou para estacionar',
  `id_parque` int(10) unsigned NOT NULL,
  `id_tipo_lugar` int(10) unsigned NOT NULL,
  `codigo_acesso` varchar(100) DEFAULT NULL COMMENT 'valor fornecido quando estado de reserva passa para aprovado',
  `hora_entrada` datetime DEFAULT NULL COMMENT 'valor fornecido quando estado de reserva passa para terminado',
  `hora_saida` datetime DEFAULT NULL COMMENT 'valor fornecido quando estado de reserva passa para terminado',
  `preco` float DEFAULT NULL COMMENT 'valor fornecido quando estado de reserva passa para terminado',
  `lugar` varchar(10) DEFAULT NULL,
  `cor` varchar(10) DEFAULT NULL,
  `andar` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parque` (`id_parque`,`id_tipo_lugar`),
  KEY `FK_reserva_veiculo` (`id_veiculo`),
  CONSTRAINT `FK_reserva_parque_tipo_lugar` FOREIGN KEY (`id_parque`, `id_tipo_lugar`) REFERENCES `parque_tipo_lugar` (`id_parque`, `id_tipo_lugar`),
  CONSTRAINT `FK_reserva_veiculo` FOREIGN KEY (`id_veiculo`) REFERENCES `veiculo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Dumping data for table parkonline.reserva: ~27 rows (approximately)
/*!40000 ALTER TABLE `reserva` DISABLE KEYS */;
INSERT INTO `reserva` (`id`, `id_veiculo`, `data_reserva`, `id_parque`, `id_tipo_lugar`, `codigo_acesso`, `hora_entrada`, `hora_saida`, `preco`, `lugar`, `cor`, `andar`) VALUES
	(1, 1, '2017-04-23 08:00:00', 1, 1, '8868e8e8-4163-11e7-8149-0800273a0b5b', '2017-04-23 08:00:00', '2017-06-06 02:53:12', 6, '', '', ''),
	(2, 1, '2017-04-20 08:00:00', 1, 1, 'b2077fb6-4163-11e7-abec-0800273a0b5b', '2017-04-20 08:00:00', '2017-04-20 09:51:00', 1.7, NULL, NULL, NULL),
	(3, 2, '2017-05-02 14:00:00', 2, 1, 'bba8b2e2-4163-11e7-8553-0800273a0b5b', '2017-05-02 15:00:00', '2017-05-02 16:30:12', 3, NULL, NULL, NULL),
	(4, 3, '2017-05-01 18:45:00', 1, 1, 'c2559060-4163-11e7-b104-0800273a0b5b', '2017-05-01 15:55:00', '2017-05-01 16:45:12', 4.5, NULL, NULL, NULL),
	(5, 4, '2017-04-20 10:21:40', 1, 1, 'ca6aad4e-4163-11e7-87a2-0800273a0b5b', '2017-04-20 12:15:00', '2017-04-20 14:30:22', 6.48, NULL, NULL, NULL),
	(6, 2, '2017-04-30 09:00:00', 1, 1, 'ce0d48a8-4163-11e7-b491-0800273a0b5b', '2017-04-30 10:15:00', '2017-04-30 14:30:22', 7.58, NULL, NULL, NULL),
	(7, 2, '2017-05-02 15:00:00', 1, 1, 'd8750f92-4163-11e7-9436-0800273a0b5b', '2017-05-02 15:45:55', '2017-05-02 17:00:00', 4.87, NULL, NULL, NULL),
	(8, 2, '2017-05-03 06:50:00', 1, 1, 'REJEITADO-e46756a2-4163-11e7-91fe-0800273a0b5b', NULL, NULL, NULL, NULL, NULL, NULL),
	(9, 2, '2017-04-26 14:00:00', 2, 1, 'ecb9d19a-4163-11e7-bb09-0800273a0b5b', '2017-04-26 14:50:00', '2017-04-26 16:30:00', 8, NULL, NULL, NULL),
	(11, 2, '2017-03-02 07:00:00', 2, 1, 'f827e31e-4163-11e7-8f63-0800273a0b5b', '2017-03-02 07:30:00', '2017-03-02 08:10:00', 2, NULL, NULL, NULL),
	(12, 2, '2017-02-01 11:00:00', 2, 1, '02a2e5d2-4164-11e7-aa67-0800273a0b5b', '2017-02-01 12:00:00', '2017-02-01 14:00:00', 10.5, NULL, NULL, NULL),
	(13, 8, '2017-05-24 21:56:49', 1, 1, '0a37a3fa-4164-11e7-802d-0800273a0b5b', '2017-05-28 21:06:25', '2017-05-28 21:06:29', 1, NULL, NULL, NULL),
	(16, 9, '2017-05-24 21:20:15', 1, 1, 'REJEITADO-147c28d6-4164-11e7-9f79-0800273a0b5b', NULL, NULL, NULL, NULL, NULL, NULL),
	(17, 1, '2017-05-24 22:20:02', 1, 1, '1c10a018-4164-11e7-b12f-0800273a0b5b', '2017-05-24 23:54:09', '2017-05-31 08:27:06', 461.8, NULL, NULL, NULL),
	(18, 8, '2017-05-25 15:58:35', 1, 1, 'f3ce5cc9-7d2c-48bf-ad6f-d63c4a37ee08', '2017-06-06 01:18:33', '2017-06-06 02:29:18', 64, '', '', ''),
	(20, 1, '2017-05-29 13:22:53', 1, 1, 'REJEITADO-dfdsfdsfsdfdsfdsf', NULL, NULL, NULL, NULL, NULL, NULL),
	(21, 10, '2017-05-29 16:08:23', 1, 1, 'a03cd4fa-c2dc-48d8-96bc-0e98adfee711', '2017-05-29 16:10:30', '2017-05-29 16:10:55', 1, '25', 'Verde', '1º'),
	(22, 10, '2017-05-29 16:51:00', 1, 1, 'REJEITADO-83943f64-cd3c-47d7-ad28-c08e85164380', NULL, NULL, NULL, NULL, NULL, NULL),
	(23, 10, '2017-05-29 17:59:57', 1, 1, 'REJEITADO-aa267ff3-e382-45b8-8eed-75d9594179de', NULL, NULL, NULL, NULL, NULL, NULL),
	(24, 11, '2017-05-29 18:00:37', 1, 1, '557167c9-1469-4048-931d-31604e46da2a', '2017-06-06 04:25:18', NULL, NULL, '', '', ''),
	(25, 1, '2017-05-30 03:39:10', 1, 1, '57a01dc7-c9ce-4868-aeef-590f2c027972', NULL, NULL, NULL, NULL, NULL, NULL),
	(26, 12, '2017-05-31 08:19:18', 1, 1, '41c8a7b2-2903-46d8-b864-9ca87711590a', '2017-05-31 08:27:00', '2017-05-31 08:30:39', 3.7, 'e52', 'cinza', '14'),
	(27, 1, '2017-06-05 16:37:01', 1, 1, '2142a93f-6696-4e3a-9055-8fe8c791f91b', '2017-06-06 01:19:25', '2017-06-06 02:29:23', 63.1, '', '', ''),
	(28, 6, '2017-06-05 17:41:28', 1, 1, 'ec0cf906-f125-4450-bcd3-a1458a8fc947', '2017-06-06 02:37:55', '2017-06-06 03:30:27', 47.8, '', '', ''),
	(29, 8, '2017-06-05 17:43:44', 1, 1, '8e8f2722-9caf-4d71-80a8-02f2e01c16aa', NULL, NULL, NULL, '', '', ''),
	(30, 8, '2017-06-05 21:54:00', 1, 1, '1a6aa0a1-2988-48b8-8af1-c8ee4077a75b', NULL, NULL, NULL, NULL, NULL, NULL),
	(31, 1, '2017-06-06 04:19:05', 1, 1, '2120c73d-19d2-46fd-b49a-06285d88dea5', NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `reserva` ENABLE KEYS */;


-- Dumping structure for table parkonline.reserva_estado
DROP TABLE IF EXISTS `reserva_estado`;
CREATE TABLE IF NOT EXISTS `reserva_estado` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_reserva` bigint(20) unsigned NOT NULL,
  `id_estado` int(10) unsigned NOT NULL,
  `dia_hora` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_reserva_id_estado` (`id_reserva`,`id_estado`),
  KEY `FK_reserva_estado_estado` (`id_estado`),
  CONSTRAINT `FK_reserva_estado_estado` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id`),
  CONSTRAINT `FK_reserva_estado_reserva` FOREIGN KEY (`id_reserva`) REFERENCES `reserva` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

-- Dumping data for table parkonline.reserva_estado: ~68 rows (approximately)
/*!40000 ALTER TABLE `reserva_estado` DISABLE KEYS */;
INSERT INTO `reserva_estado` (`id`, `id_reserva`, `id_estado`, `dia_hora`) VALUES
	(1, 1, 1, '2017-04-19 12:30:00'),
	(2, 1, 2, '2017-04-19 12:30:02'),
	(3, 1, 3, '2017-04-19 12:35:00'),
	(5, 2, 1, '2017-04-20 12:00:00'),
	(6, 2, 2, '2017-04-20 12:00:02'),
	(7, 2, 3, '2017-04-20 12:02:00'),
	(8, 1, 5, '2017-04-29 10:00:00'),
	(10, 6, 2, '2017-04-30 09:02:00'),
	(11, 7, 2, '2017-05-02 15:05:00'),
	(12, 8, 2, '2017-05-03 13:45:00'),
	(13, 4, 1, '2017-05-24 09:09:51'),
	(16, 4, 2, '2017-05-24 13:13:01'),
	(17, 4, 3, '2017-05-24 13:36:16'),
	(18, 13, 2, '2017-05-24 21:56:49'),
	(19, 16, 2, '2017-05-24 21:20:15'),
	(20, 17, 2, '2017-05-24 22:20:02'),
	(21, 6, 3, '2017-05-24 23:49:11'),
	(22, 7, 3, '2017-05-24 23:49:51'),
	(23, 8, 4, '2017-05-24 23:52:10'),
	(24, 6, 5, '2017-05-24 23:52:22'),
	(25, 4, 5, '2017-05-24 23:52:34'),
	(26, 13, 3, '2017-05-24 23:52:50'),
	(27, 17, 3, '2017-05-24 23:53:08'),
	(28, 17, 5, '2017-05-24 23:54:09'),
	(29, 6, 6, '2017-05-24 23:55:40'),
	(30, 4, 6, '2017-05-24 23:56:03'),
	(31, 2, 5, '2017-05-24 23:56:30'),
	(32, 16, 4, '2017-05-25 15:31:04'),
	(33, 18, 2, '2017-05-25 15:58:35'),
	(43, 18, 3, '2017-05-28 21:03:51'),
	(44, 7, 5, '2017-05-28 21:06:21'),
	(45, 13, 5, '2017-05-28 21:06:25'),
	(46, 2, 6, '2017-05-28 21:06:28'),
	(47, 13, 6, '2017-05-28 21:06:29'),
	(48, 7, 6, '2017-05-28 21:06:31'),
	(49, 20, 2, '2017-05-29 13:22:53'),
	(50, 21, 2, '2017-05-29 16:08:23'),
	(51, 21, 3, '2017-05-29 16:09:46'),
	(52, 21, 5, '2017-05-29 16:10:30'),
	(53, 21, 6, '2017-05-29 16:10:55'),
	(54, 22, 2, '2017-05-29 16:51:00'),
	(55, 23, 2, '2017-05-29 17:59:57'),
	(56, 24, 2, '2017-05-29 18:00:37'),
	(57, 25, 2, '2017-05-30 03:39:10'),
	(58, 26, 2, '2017-05-31 08:19:18'),
	(59, 26, 3, '2017-05-31 08:26:28'),
	(60, 26, 5, '2017-05-31 08:27:00'),
	(61, 17, 6, '2017-05-31 08:27:06'),
	(62, 20, 4, '2017-05-31 08:28:58'),
	(63, 22, 4, '2017-05-31 08:28:59'),
	(64, 23, 4, '2017-05-31 08:29:24'),
	(65, 26, 6, '2017-05-31 08:30:39'),
	(66, 27, 2, '2017-06-05 16:37:01'),
	(67, 27, 3, '2017-06-05 16:40:33'),
	(68, 28, 2, '2017-06-05 17:41:28'),
	(69, 28, 3, '2017-06-05 17:42:00'),
	(89, 18, 5, '2017-06-06 01:18:33'),
	(90, 27, 5, '2017-06-06 01:19:25'),
	(91, 18, 6, '2017-06-06 02:29:18'),
	(92, 27, 6, '2017-06-06 02:29:23'),
	(93, 24, 3, '2017-06-06 02:37:50'),
	(94, 28, 5, '2017-06-06 02:37:55'),
	(95, 1, 6, '2017-06-06 02:53:12'),
	(98, 28, 6, '2017-06-06 03:30:27'),
	(100, 29, 2, '2017-06-06 04:56:52'),
	(101, 30, 2, '2017-06-05 21:54:00'),
	(102, 31, 2, '2017-06-06 04:19:05'),
	(103, 24, 5, '2017-06-06 04:25:18');
/*!40000 ALTER TABLE `reserva_estado` ENABLE KEYS */;


-- Dumping structure for table parkonline.tipo_lugar
DROP TABLE IF EXISTS `tipo_lugar`;
CREATE TABLE IF NOT EXISTS `tipo_lugar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table parkonline.tipo_lugar: ~5 rows (approximately)
/*!40000 ALTER TABLE `tipo_lugar` DISABLE KEYS */;
INSERT INTO `tipo_lugar` (`id`, `descricao`) VALUES
	(1, 'Normal'),
	(2, 'Incapacitado'),
	(3, 'Gravida'),
	(4, 'Idoso'),
	(5, 'Eletrico');
/*!40000 ALTER TABLE `tipo_lugar` ENABLE KEYS */;


-- Dumping structure for table parkonline.utilizador
DROP TABLE IF EXISTS `utilizador`;
CREATE TABLE IF NOT EXISTS `utilizador` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `senha` varchar(50) NOT NULL,
  `authenticationKey` varchar(100) DEFAULT NULL,
  `accessToken` varchar(100) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `tipoUtilizador` enum('Admin','Condutor','Parque') NOT NULL DEFAULT 'Condutor',
  `id_parque` int(11) DEFAULT NULL,
  `nif` varchar(9) NOT NULL,
  `apelido` varchar(20) NOT NULL,
  `morada` varchar(50) NOT NULL,
  `rememberMe` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `nome` (`nome`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- Dumping data for table parkonline.utilizador: ~11 rows (approximately)
/*!40000 ALTER TABLE `utilizador` DISABLE KEYS */;
INSERT INTO `utilizador` (`id`, `nome`, `senha`, `authenticationKey`, `accessToken`, `email`, `telefone`, `tipoUtilizador`, `id_parque`, `nif`, `apelido`, `morada`, `rememberMe`) VALUES
	(1, 'Maria', 'qwe123', 'xxggzGN5WQI9rDloVtXfvsLLG3MuvZDo', '', 'mara@gggg.com', '', 'Condutor', NULL, '111222333', 'Salvador', '1120', 0),
	(2, 'vitor', 'qwe123', 'ngggzGN5WQI9rDloVtXfvsLLG3MuvZDo', '', 'vitorcool@gmail.com', NULL, 'Admin', NULL, '192195120', 'Oliveira', '', 0),
	(4, 'ines', 'qwerty1234', 'SrQ2iqropnKm2y32wLE7Y8mgMLGsKhmM', NULL, 'ines.p.pinho@gmil.com', NULL, 'Admin', NULL, '192340921', 'Pinho', '', 0),
	(6, 'Taslima', '12345abcd', NULL, NULL, 'taslima_abedin@hotmail.com', '963333333', 'Condutor', NULL, '123456789', 'Abedin', 'Avenida Rio de Janeiro', 0),
	(7, 'Daniyal', 'Dani007', NULL, NULL, 'Dani_Teixeira@gmail.com', NULL, 'Condutor', NULL, '987456321', 'Teixeira', 'Massamá', 0),
	(8, 'André', 'Andr3111', NULL, NULL, 'ANDRE_SILVESTRE1992@HOTMAIL.COM', '937777777', 'Condutor', NULL, '852852963', 'Silvestre', 'Rua das flores', 0),
	(9, 'Ricardo', 'ricardo7', NULL, NULL, 'ricardotrindade@hotmail.com', NULL, 'Condutor', NULL, '741741147', 'Trindade', 'Avenida rio de Janeiro', 0),
	(10, 'Marisa', 'Mari200', NULL, NULL, 'marisa_marisa@hotmail.com', '928887575', 'Condutor', NULL, '121342582', 'Ramos', 'Avenida da Republica', 0),
	(11, 'taslima2', '1234', 'uurIdBckCT6cxArVA12UVUPtuxFZuMfl', NULL, 'Taslima_a@hotmail.com', '969999999', 'Condutor', NULL, '999999999', 'Abedin', 'Gjaghss', 0),
	(12, 'p_restauradores', 'qwe123', 'WQGr8jpH2McKvLTMafPbwB6aUitFRPMf', NULL, 'p_restauradores@gmail.com', '', 'Parque', 1, '222333444', 'parque', '.', 0),
	(13, 'José', 'qwe123', 'a_REV70Z1mJIhRnfBReWiw_u5DfvFlrs', NULL, 'jose_alberto@mail.pt', '96', 'Condutor', NULL, '192184002', 'Alberto', 'Torres Vedras', 0);
/*!40000 ALTER TABLE `utilizador` ENABLE KEYS */;


-- Dumping structure for table parkonline.veiculo
DROP TABLE IF EXISTS `veiculo`;
CREATE TABLE IF NOT EXISTS `veiculo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_utilizador` int(11) unsigned NOT NULL,
  `matricula` varchar(20) NOT NULL,
  `marca` varchar(20) NOT NULL,
  `n_lugares` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_veiculo_utilizador` (`id_utilizador`),
  CONSTRAINT `FK_veiculo_utilizador` FOREIGN KEY (`id_utilizador`) REFERENCES `utilizador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table parkonline.veiculo: ~12 rows (approximately)
/*!40000 ALTER TABLE `veiculo` DISABLE KEYS */;
INSERT INTO `veiculo` (`id`, `id_utilizador`, `matricula`, `marca`, `n_lugares`) VALUES
	(1, 2, '11-aa-22', 'peugeot', 5),
	(2, 4, '85-NB-43', 'Golf', 5),
	(3, 10, '22-FD-85', 'Audi', 5),
	(4, 8, '32-JL-41', 'Dacia', 2),
	(5, 7, '34-KL-10', 'Citroen', 2),
	(6, 6, '53-NS-47', 'Fiat', 5),
	(7, 9, '23-XP-55', 'Kia', 5),
	(8, 1, '24-TH-10', 'Mercedes', 5),
	(9, 11, '34-ER-23', 'Nissan', 5),
	(10, 13, '89-DF-27', 'BMW', 5),
	(11, 13, '89-EQ-01', 'Toyota', 5),
	(12, 4, '49-LX-69', 'Fiat', 5);
/*!40000 ALTER TABLE `veiculo` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
