CREATE DEFINER=`root`@`%` PROCEDURE `factura_tmp_table_items`(IN `ano` INT, IN `mes` INT, IN `id_condutor` INT, IN `tbl_name` VARCHAR(50))
	LANGUAGE SQL
	NOT DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY DEFINER
	COMMENT ''
BEGIN
	SET @tableName = tbl_name;
	
	/* drop temporary table*/
	SET @q = CONCAT('
	DROP TABLE IF EXISTS `',@tableName,'`;');
	PREPARE stmt FROM @q;
   EXECUTE stmt;
	
	/* create temporary table*/
	SET @q = CONCAT('
	CREATE TEMPORARY TABLE `',@tableName,'` (','
	
	SELECT 
		f.id as id_fatura,f.data_emissao as data_emissao_fatura,f.mes_faturacao,
		re3.id_reserva,r.data_reserva,
		1 as taxa_reserva,r.preco,
		r.hora_entrada,r.hora_saida,TIMESTAMPDIFF(MINUTE,r.hora_entrada,r.hora_saida) as tempo_minutos,
	   v.matricula,v.marca,
	   p.nome,p.morada,p.cod_postal,p.preco_min,
	   tp.descricao as tipo_lugar
	FROM reserva_estado re3 
	INNER JOIN reserva as r
	  ON r.id=re3.id_reserva
	INNER JOIN veiculo v
	  ON v.id=r.id_veiculo
	INNER JOIN utilizador u
	  ON u.id=v.id_utilizador
	 AND u.id=',id_condutor,'
	INNER JOIN parque p
	  ON p.id=r.id_parque
	INNER JOIN tipo_lugar tp
	  ON tp.id=r.id_tipo_lugar
	INNER JOIN factura as f
	  ON YEAR(r.hora_entrada)=',ano,' 
	  AND MONTH(r.hora_entrada)=',mes,'
	LEFT JOIN reserva_estado re5
		ON re5.id_reserva=re3.id_reserva
		AND re5.id_estado=5	
	WHERE re3.id_estado=3
	
	',')');
	
	 PREPARE stmt FROM @q;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
       
END