select 
	f.id as id_fatura,f.data_emissao as data_emissao_fatura,f.mes_faturacao,
	re3.id_reserva,r.data_reserva,
	
	1 as taxa_reserva,r.preco,
	r.hora_entrada,r.hora_saida,TIMESTAMPDIFF(MINUTE,r.hora_entrada,r.hora_saida) as tempo_minutos,
	
   v.matricula,v.marca,
   p.nome,p.morada,p.cod_postal,p.preco_min,
   tp.descricao as tipo_lugar
from reserva_estado re3 

inner join reserva as r
  on r.id=re3.id_reserva
inner join veiculo v
  on v.id=r.id_veiculo
inner join parque p
  on p.id=r.id_parque
inner join tipo_lugar tp
  on tp.id=r.id_tipo_lugar
  
inner join factura as f
  on YEAR(r.hora_entrada)=YEAR(f.mes_faturacao) and MONTH(r.hora_entrada)=MONTH(f.mes_faturacao)
  
LEFT JOIN reserva_estado re5
	ON re5.id_reserva=re3.id_reserva
	AND re5.id_estado=5
	
where re3.id_estado=3