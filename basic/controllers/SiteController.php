<?php

namespace app\controllers;

use app\models\Reserva;
use app\models\Veiculo;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Parque;
use app\models\ParqueTipoLugar;
use yii\web\AssetManager;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'access2' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['mostraparque'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->getIdentity()->tipoUtilizador=='Condutor' ||
                            Yii::$app->user->getIdentity()->tipoUtilizador=='Admin' ;
                        }
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionOnlineparks(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = Parque::find()->asArray()->all();
        return [
                'message'=>'ok',
                'parks'  =>$model
            ];
    }

    public function actionMostraparque(){
        $r = Yii::$app->request;
        /*
        $parqueId = $r->get('id');
        $origem   = $r->get('origem');
        $tempo1   = $r->get('tempo1');
        $distancia1= $r->get('distancia1');
        $destino   = $r->get('destino');
        $tempo2   = $r->get('tempo2');
        $distancia2= $r->get('distancia2');*/

        $parque = Parque::find()
                    ->innerJoin('parque_tipo_lugar','parque_tipo_lugar.id_parque=parque.id')
                    ->select('parque.*,sum(parque_tipo_lugar.total_lugares) as total_lugares')
                    ->where(['parque.id'=>$r->get('id'),
                    //         'parque_tipo_lugar.id_tipo_lugar'=>1
                    ])
                    ->asArray()->one();

        $lugares = ParqueTipoLugar::find()
                    ->innerJoin('tipo_lugar','tipo_lugar.id=parque_tipo_lugar.id_tipo_lugar')
                    ->select('parque_tipo_lugar.*,tipo_lugar.descricao')
                    ->where(['parque_tipo_lugar.id_parque'=>$r->get('id')])
                    ->asArray()->all();

        $veiculos = Veiculo::find()
                    ->where(['veiculo.id_utilizador'=>Yii::$app->user->id])
                    ->asArray()->all();


        $reserva = new Reserva();


        $reserva->data_reserva = date('Y-m-d H:i:s');;
        $reserva->id_parque= $r->get('id');

        return $this->render('mostraparque',[
            'get'=>$r->get(),
            'reserva'=>$reserva,
            'parque'=>$parque,
            'lugares'=>$lugares,
            'veiculos'=>$veiculos
        ]);
    }

    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect('site/login');
        }else {
            if(Yii::$app->user->getIdentity()->tipoUtilizador=='Parque'){
                return $this->redirect('/parque/operacoes');
            }else {
                return $this->render('index');
            }
        }
    }


    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $p = Yii::$app->request->post();
        if ($model->load($p)){
            if($model->login()) {
                return $this->redirect('/');
            }
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }



    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionMeuslocais()
    {
        return $this->render('meuslocais');
    }


    public function actionFaturas()
    {
        return $this->render('2faturas');
    }

}

