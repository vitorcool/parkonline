<?php

namespace app\controllers;

use app\models\ParqueTipoLugar;
use app\models\Reserva;
use app\models\ReservaEstado;
use app\models\TipoLugar;
use Faker\Provider\DateTime;
use Rhumsaa\Uuid\Exception\UnsatisfiedDependencyException;
use Rhumsaa\Uuid\Uuid;
use Yii;
use app\models\Parque;
use app\models\ParqueSearch;
use app\controllers\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ParqueController implements the CRUD actions for Parque model.
 */
class ParqueController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['operacoes','reserva_confirmar','reserva_rejeitar','reserva_entrar','reserva_sair'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->getIdentity()->tipoUtilizador=='Parque';
                        }
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'access2' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['reserva_pedido','enviado_pedido'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->getIdentity()->tipoUtilizador=='Condutor' ||
                                   Yii::$app->user->getIdentity()->tipoUtilizador=='Admin' ;
                        }
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'access3' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','view','create','update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->getIdentity()->tipoUtilizador=='Admin';
                        }
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }



    /**
     * Lists all reserve operations (pendente[confirmar,rejeitar], confirmado[entrar], dentro[sair]).
     * @return mixed
     */
    public function actionOperacoes()
    {
        $parqueId = Yii::$app->user->getIdentity()->id_parque;

        $ultimosSql = ReservaEstado::find()
                    ->select(['reserva_estado.id_reserva as id','max(reserva_estado.id) as id_reserva_estado'])
                    ->groupBy('reserva_estado.id_reserva')
                    ->createCommand()->rawSql;

        $pendentes = Reserva::find()
            ->joinWith(['reservaEstados','idVeiculo','idVeiculo.idUtilizador'],true,'INNER JOIN')
            ->innerJoin('('.$ultimosSql.') as ultimos','ultimos.id_reserva_estado=reserva_estado.id')
            ->select(['reserva.*',
                'veiculo.matricula',
                'veiculo.marca',
                'utilizador.nome',
                'utilizador.apelido',
                'utilizador.nif',
                'reserva_estado.id as reserva_estado_id',
                'reserva_estado.dia_hora as estado_reserva_data'])
            ->where(['reserva.id_parque'=>$parqueId,
                     'reserva_estado.id_estado'=>Reserva::Reserva_Pendente
            ])
            ->asArray()->all();

        $confirmadas= Reserva::find()
            ->joinWith(['reservaEstados','idVeiculo','idVeiculo.idUtilizador'],'INNER JOIN')
            ->innerJoin('('.$ultimosSql.') as ultimos','ultimos.id_reserva_estado=reserva_estado.id')
            ->select(['reserva.*',
                      'veiculo.matricula',
                      'veiculo.marca',
                      'utilizador.nome',
                      'utilizador.apelido',
                      'utilizador.nif',
                      'reserva_estado.id as reserva_estado_id',
                      'reserva_estado.dia_hora as estado_reserva_data'])
            ->where(['reserva.id_parque'=>$parqueId,
                'reserva_estado.id_estado'=>Reserva::Reserva_Aprovado])
            ->asArray()->all();

        $dentro = Reserva::find()
            ->joinWith(['reservaEstados','idVeiculo','idVeiculo.idUtilizador'],'INNER JOIN')
            ->innerJoin('('.$ultimosSql.') as ultimos','ultimos.id_reserva_estado=reserva_estado.id')
            ->select(['reserva.*',
                'veiculo.matricula',
                'veiculo.marca',
                'utilizador.nome',
                'utilizador.apelido',
                'utilizador.nif',
                'reserva_estado.id as reserva_estado_id',
                'reserva_estado.dia_hora as estado_reserva_data'])
            ->where(['reserva.id_parque'=>$parqueId,
                'reserva_estado.id_estado'=>Reserva::Reserva_Dentro])
            ->asArray()->all();

        return $this->render('operacoes', [
            'pendentes'   => $pendentes,
            'confirmadas' => $confirmadas,
            'dentro'      => $dentro,
        ]);
    }

    /**
     * display message to driver telling that reserve is being processed by selected online park
     * @return mixed
     */
    public function actionEnviado_pedido()
    {
        $parqueId = Yii::$app->user->getIdentity()->id_parque;
        $reservaId= Yii::$app->request->get('id');

        $reserva=Reserva::getReservaEstado($reservaId)->one();
        return $this->render('enviado_pedido',['reserva'=>$reserva]);
    }

    /**
     * add reserve with state equal to "pedido",and than set state to "pendente"
     * @return mixed
     */
    public function actionReserva_pedido()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $userid = Yii::$app->user->id;

        $reserva = new Reserva();
        $reserva->setAttributes( Yii::$app->request->get() );
        do {
            try {
                $reserva->codigo_acesso = Uuid::uuid4().'';
            } catch (UnsatisfiedDependencyException $e) {
                //echo 'Caught exception: ' . $e->getMessage() . "\n";
            }
        } while( ! $reserva->validate(['codigo_acesso']) );



        if($reserva->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $reserva->save();
                $estado = new ReservaEstado();
                $estado->id_reserva = $reserva->id;
                $estado->id_estado = Reserva::Reserva_Pendente;
                $estado->dia_hora = $reserva->data_reserva;
                if($estado->save()) {
                    $transaction->commit();
                }else {
                    return [
                        'message' => $estado->getFirstErrors(),
                    ];
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }

            return [
                'message' => 'ok',
                'reserva' => $reserva
                ];
        }else{
            return [
                'message' => $reserva->getFirstErrors(),
            ];
        }

    }

    /**
     * change reserve state to "aprovado"
     * @return mixed
     */
    public function actionReserva_confirmar()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $r = yii::$app->request->get();
        $userid = Yii::$app->user->id;
        $reservaId = yii::$app->request->get('id');

        // valida mudança de estado
        $modelEstado=Reserva::getReservaEstado($reservaId)->asArray()->one();
        if(!isset($modelEstado))
            return ['message'=>'Reserva com id '.$reservaId.' não existe'];
        else if( $modelEstado['reserva_estado_estado_id'] != Reserva::Reserva_Pendente )
            return ['message'=>'Estado da reserva id '.$reservaId.
                   ' não pode ser alterado para "Aprovado". Reserva estã no estado "'.$modelEstado['reserva_estado_estado_desc'].'"'];

        $reserva = Reserva::find()->where([
            'id'=>$reservaId
        ])->one();
        $reserva->setAttributes($r);
        if($reserva->validate(['codigo_acesso','andar','cor','lugar']))
            $reserva->save();
        else
            return ['message'=>reset($reserva->getFirstErrors())];

        $model = new ReservaEstado();
        $model->id_reserva = $reservaId;
        $model->id_estado  = Reserva::Reserva_Aprovado;
        $model->dia_hora   = date('Y-m-d H:i:s');
        $model->save();
        return ['message'=>'ok'];
    }

    /**
     * change reserve state to "rejeitado"
     * @return mixed
     */
    public function actionReserva_rejeitar()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $userid = Yii::$app->user->id;
        $reservaId = yii::$app->request->get('id');

        // valida mudança de estado
        $modelEstado=Reserva::getReservaEstado($reservaId)->asArray()->one();
        if(!isset($modelEstado))
            return ['message'=>'Reserva com id '.$reservaId.' não existe'];
        else if( $modelEstado['reserva_estado_estado_id'] != Reserva::Reserva_Pendente )
            return ['message'=>'Estado da reserva id '.$reservaId.
                ' não pode ser alterado para "Aprovado". Reserva estã no estado "'.$modelEstado['reserva_estado_estado_desc'].'"'];

        $model = new ReservaEstado();
        $model->id_reserva = $reservaId;
        $model->id_estado  = Reserva::Reserva_Rejeitado;
        $model->dia_hora   = date('Y-m-d H:i:s');
        $model->save();
        return ['message'=>'ok'];
    }

    /**
     * change reserve state to "dentro"
     * @return mixed
     */
    public function actionReserva_entrar()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $userid = Yii::$app->user->id;
        $reservaId = yii::$app->request->get('id');

        // valida mudança de estado
        $modelEstado=Reserva::getReservaEstado($reservaId)->asArray()->one();
        if(!isset($modelEstado))
            return ['message'=>'Reserva com id '.$reservaId.' não existe'];
        else if( $modelEstado['reserva_estado_estado_id'] != Reserva::Reserva_Aprovado)
            return ['message'=>'Estado da reserva id '.$reservaId.
                ' não pode ser alterado para "Aprovado". Reserva estã no estado "'.$modelEstado['reserva_estado_estado_desc'].'"'];


        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = new ReservaEstado();
            $model->id_reserva = $reservaId;
            $model->id_estado  = Reserva::Reserva_Dentro;
            $model->dia_hora   = date('Y-m-d H:i:s');
            $model->save();

            $dat = \DateTime::createFromFormat('Y-m-d H:i:s', $model->dia_hora);
            $reserva = Reserva::findOne( $model->id_reserva);
            $reserva->hora_entrada = date('Y-m-d H:i:s',$dat->getTimestamp());
            $reserva->save();
            $transaction->commit();
            return ['message'=>'ok'];

        } catch (\Exception $e) {
            $transaction->rollBack();
            return [
                'message' => $e->getMessage(),
            ];
        }
    }

    /**
     * change reserve state to "fora"
     * @return mixed
     */
    public function actionReserva_sair()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $userid = Yii::$app->user->id;
        $reservaId = yii::$app->request->get('id');

        // valida mudança de estado
        $modelEstado=Reserva::getReservaEstado($reservaId)->asArray()->one();
        if(!isset($modelEstado))
            return ['message'=>'Reserva com id '.$reservaId.' não existe'];
        else if( $modelEstado['reserva_estado_estado_id'] != Reserva::Reserva_Dentro )
            return ['message'=>'Estado da reserva id '.$reservaId.
                ' não pode ser alterado para "Aprovado". Reserva estã no estado "'.$modelEstado['reserva_estado_estado_desc'].'"'];

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = new ReservaEstado();
            $model->id_reserva = $reservaId;
            $model->id_estado  = Reserva::Reserva_Fora;
            $model->dia_hora   = date('Y-m-d H:i:s');
            $model->save();

            $dat = \DateTime::createFromFormat('Y-m-d H:i:s', $model->dia_hora);
            $reserva = Reserva::findOne( $model->id_reserva);
            $reserva->hora_saida = date('Y-m-d H:i:s',$dat->getTimestamp());

            $entrada = $reserva->findEstado(Reserva::Reserva_Dentro);
            $entrada_data = \DateTime::createFromFormat('Y-m-d H:i:s', $entrada->dia_hora);
            $dif = $dat->diff($entrada_data);
            $minutos = $dif->h*60+$dif->i;
            $reserva->preco = yii::$app->params['taxa_reserva'] +
                                ($minutos*$reserva->idParqueTipoLugar->idParque->preco_min);
            $reserva->save();
            $transaction->commit();
            return ['message'=>'ok'];
        } catch (\Exception $e) {
            $transaction->rollBack();
            return [
                'message' => $e->getMessage(),
            ];
        }
    }


    /**
     * Lists all Parque models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ParqueSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Parque model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Parque model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Parque();

        if ($model->load(Yii::$app->request->post())){
            $model->gps = 'x';
            if ($model->validate()) {
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Parque model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Parque model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Parque model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Parque the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Parque::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    const MIN_SEARCH_LEN=0;
    public function actionAutocomplete(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $value    =Yii::$app->getRequest()->getQueryParam('value');
        $offset   =Yii::$app->getRequest()->getQueryParam('offset');
        $minLength =self::MIN_SEARCH_LEN;
        if($minLength>0 && ($value=='' || strlen($value)<$minLength)) {
            return [__METHOD__.' precisa pelo menos '.$minLength.' caracteres, para proceder'];
        }

        $ret=array();

        $model = Parque::find()
            ->select("id,nome,localidade");
        if( !empty($id) || is_numeric($value)) {
            $val=!empty($id) ? $id : $value;
            $model = $model->where( ["id"=> intval($val)] )->all();
        }else
            //if (!isset($model) && empty($id))
        {
            $offset= $offset==''
                ? strlen($value)
                : (intval($offset)<$minLength
                    ? $minLength
                    : intval($offset));
            $val=substr($value,0,$offset);

            $model = $model->where( $val==''?'':['like',"concat(nome,' ',localidade)" ,"$val"] )->all();
        }
        if ($model !== null) {
            foreach($model as $k=>$rec) {
                $ret[] = array(
                    'label'=>$rec['id'].' - '.$rec['nome'].' / '.$rec['localidade'],
                    'value'=>$rec['id'],
                );
            }
        }
        return $ret;
    }


    public function actionAutocomplete_tipolugar($id_parque){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $id    =Yii::$app->getRequest()->getQueryParam('id');
        $value    =Yii::$app->getRequest()->getQueryParam('value');
        $offset   =Yii::$app->getRequest()->getQueryParam('offset');
        $minLength =self::MIN_SEARCH_LEN;
        if($minLength>0 && ($value=='' || strlen($value)<$minLength)) {
            return [__METHOD__.' precisa pelo menos '.$minLength.' caracteres, para proceder'];
        }

        $ret=array();

        $model = TipoLugar::find()
                    ->innerJoin('parque_tipo_lugar','parque_tipo_lugar.id_tipo_lugar=tipo_lugar.id')
            ->select(["tipo_lugar.id","tipo_lugar.descricao"])
            ->Where(['parque_tipo_lugar.id_parque'=>$id_parque]);
        if( !empty($id) || is_numeric($value)) {
            $val=!empty($id) ? $id : $value;
            $model = $model->andWhere( ["tipo_lugar.id"=> intval($val)] )->all();
        }else
            //if (!isset($model) && empty($id))
        {
            $offset= $offset==''
                ? strlen($value)
                : (intval($offset)<$minLength
                    ? $minLength
                    : intval($offset));
            $val=substr($value,0,$offset);

            $model = $model->andWhere($val==''?'':['like',"concat(tipo_lugar.descricao)" ,"$val"] )->all();
        }
        if ($model !== null) {
            foreach($model as $k=>$rec) {
                $ret[] = array(
                    'label'=>$rec['id'].' - '.$rec['descricao'],
                    'value'=>$rec['id'],
                );
            }
        }
        return $ret;
    }

}


