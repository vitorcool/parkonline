<?php

namespace app\controllers;

use app\models\Veiculo;
use Yii;
use app\models\Veiculo2;
use app\models\Veiculo2Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Connection;

/**
 * VeiculoController implements the CRUD actions for veiculo model.
 */
class VeiculoController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->getIdentity()->tipoUtilizador=='Admin';
                        }
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],

    
            'access2' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'view','create','update','delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->getIdentity()->tipoUtilizador=='Admin'
                                    ||
                                   Yii::$app->user->getIdentity()->tipoUtilizador=='Condutor';
                        }
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
            
        ];
    }

    /**
     * Lists all veiculo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Veiculo2Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,false);

        return $this->render('index', [
            'asAdmin'     =>false,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all veiculo models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $searchModel = new Veiculo2Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,yii::$app->user->getIdentity()->tipoUtilizador=='Admin');

        return $this->render('index', [
            'asAdmin'     => yii::$app->user->getIdentity()->tipoUtilizador=='Admin',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single veiculo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if(yii::$app->user->getIdentity()->tipoUtilizador!='Admin') {
            if (yii::$app->user->getIdentity()->tipoUtilizador == 'Condutor' && $model->id_utilizador != yii::$app->user->id)
                throw new ForbiddenHttpException(Yii::t('yii', 'Acesso negado.'));
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new veiculo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new veiculo2();
        if(yii::$app->user->getIdentity()->tipoUtilizador=='Condutor' || yii::$app->request->get('admin') == false)
            $model->id_utilizador = yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'asAdmin'=> yii::$app->request->get('admin'),
            ]);
        }
    }

    /**
     * Updates an existing veiculo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if(yii::$app->user->getIdentity()->tipoUtilizador=='Condutor' && $model->id_utilizador!=yii::$app->user->id)
            throw new ForbiddenHttpException(Yii::t('yii', 'Acesso negado.'));


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'asAdmin'=> yii::$app->request->get('admin'),
            ]);
        }
    }

    /**
     * Deletes an existing veiculo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        if(yii::$app->user->getIdentity()->tipoUtilizador=='Condutor' && $model->id_utilizador!=yii::$app->user->id)
            throw new ForbiddenHttpException(Yii::t('yii', 'Acesso negado.'));

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the veiculo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return veiculo2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = veiculo2::find(true)->where(['veiculo.id'=>$id])->one();
        if ($model!== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    const MIN_SEARCH_LEN=0;
    public function actionAutocomplete(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $value    =Yii::$app->getRequest()->getQueryParam('value');
        $offset   =Yii::$app->getRequest()->getQueryParam('offset');
        $minLength =self::MIN_SEARCH_LEN;
        if($minLength>0 && ($value=='' || strlen($value)<$minLength)) {
            return [__METHOD__.' precisa pelo menos '.$minLength.' caracteres, para proceder'];
        }

        $ret=array();

        $model = Veiculo::find()
            ->select("id,matricula,marca,n_lugares");
        if( !empty($id) || is_numeric($value)) {
            $val=!empty($id) ? $id : $value;
            $model = $model->where( ["id"=> intval($val)] )->all();
        }else
        //if (!isset($model) && empty($id))
        {
            $offset= $offset==''
                ? strlen($value)
                : (intval($offset)<$minLength
                    ? $minLength
                    : intval($offset));
            $val=substr($value,0,$offset);

            $model = $model->where( $val==''?'':['like',"concat(matricula,' ',marca)" ,"$val"] )->all();
        }
        if ($model !== null) {
            foreach($model as $k=>$rec) {
                $ret[] = array(
                    'label'=>$rec['id'].' - '.$rec['marca'].' '.$rec['n_lugares'].' lugares / '.$rec['matricula'],
                    'value'=>$rec['id'],
                );
            }
        }
        return $ret;
    }
}
