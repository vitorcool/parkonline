<?php

namespace app\controllers;

use app\models\Estado;
use Yii;
use app\models\Reserva;
use app\models\ReservaSearch;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReservaController implements the CRUD actions for Reserva model.
 */
class ReservaController extends Controller
{
    public function behaviors()
    {
        return [

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                  //  'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['delete','update'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->getIdentity()->tipoUtilizador=='Admin';
                        }
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'access2' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','view','view2','estado'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return true;//Yii::$app->user->getIdentity()->tipoUtilizador=='Parque';
                        }
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Reserva models.
     * @return mixed
     */
    public function actionIndex()
    {
        $userId = yii::$app->user->id;
        $parqueId = $parqueId = Yii::$app->user->getIdentity()->tipoUtilizador=='Parque'? Yii::$app->user->getIdentity()->id_parque:null;

        $searchModel = new ReservaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,array_merge(
            is_null($parqueId)?[]:['reserva.id_parque'=>$parqueId],
            Yii::$app->user->getIdentity()->tipoUtilizador=='Condutor'?['utilizador.id'=>$userId]:[]
        ));

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Reserva models.
     * @return mixed
     */
    public function actionEstado($estado)
    {
        $userId = yii::$app->user->id;
        $model= Estado::find()->where(['descricao'=>$estado])->one();
        if($model==null)
            throw new \yii\web\HttpException(404, yii::t('app','Estado de reserva desconhecido.'));

        $idEstado = $model->id;


        $searchModel = new ReservaSearch();
        $dataProvider = $searchModel->search(  Yii::$app->request->queryParams,
                        yii::$app->user->getIdentity()->tipoUtilizador=='Parque'
                            ?[
                                'reserva_estado.id_estado'=>$idEstado,
                                'reserva.id_parque'=>yii::$app->user->getIdentity()->id_parque
                            ]
                            :[
                                'reserva_estado.id_estado'=>$idEstado,
                                'utilizador.id'=>$userId
                            ]
                );

        return $this->render('index', [
            'title' => yii::t('app','Reservas estado {estado}',['estado'=>$model->descricao]),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    /**
     * Displays a single Reserva model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single Reserva model.
     * @param string $id
     * @return mixed
     */
    public function actionView2($id)
    {
        return $this->render('view2', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Reserva model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
/*    public function actionCreate()
    {
        $model = new Reserva();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
*/
    /**
     * Updates an existing Reserva model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Reserva model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Reserva model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Reserva the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reserva::getReservaEstado($id)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
