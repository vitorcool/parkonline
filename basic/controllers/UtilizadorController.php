<?php

namespace app\controllers;

use app\models\LoginForm;
use app\models\Veiculo;
use Rhumsaa\Uuid\Uuid;
use Yii;
use app\models\Utilizador;
use app\models\UtilizadorSearch;
use yii\swiftmailer\Mailer;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UtilizadorController implements the CRUD actions for Utilizador model.
 */
class UtilizadorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access1' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['create','sendmail','confirma'],
                'rules' => [
                    //'actions' => ['create', 'sendmail'],
                    [
                        'allow' => true,
                    ],
                ],
            ],
            'access2' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return Yii::$app->user->getIdentity()->tipoUtilizador=='Admin';
                        }
                    ],
                    [
                        'actions' => ['index', 'delete'],
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'access3' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['view', 'update'],
                'rules' => [
                    [
                        'actions' => ['view', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $request = Yii::$app->request;
                            $get = $request->get();
                            $id = $request->get('id');
                            return Yii::$app->user->getIdentity()->tipoUtilizador=='Admin'
                                    ||
                                   Yii::$app->user->id==$id;
                        }
                    ]
                ]
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
              //      'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Utilizador models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UtilizadorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Utilizador model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Utilizador model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Utilizador();
        $veiculo = new Veiculo();

        $model->authenticationKey = Uuid::uuid4().'';
        $loaded = $model->load(Yii::$app->request->post());

        if(yii::$app->user->isGuest) {
            $loaded = $loaded && $veiculo->load(Yii::$app->request->post());
            $model->estado = 'pendente';
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($loaded && $model->save()) {
                if (yii::$app->user->isGuest) {
                    $veiculo->id_utilizador = $model->id;
                    if ($veiculo->save()) {
                        $transaction->commit();
                        $this->sendmail($model);
                        return $this->redirect("/");
                    }
                }else{
                    $transaction->commit();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
        return $this->render('create', [
            'model' => $model,
            'veiculo'=>$veiculo,
        ]);

    }


    public function actionConfirma($auth){
        $model = Utilizador::find()->where(['authenticationKey'=>$auth])->one();

        if($model){
            if($model->estado!='pendente'){
                throw new NotFoundHttpException( yii::t('app','Email do utilizador {nome} já foi confirmado.',['nome'=>$model->getNomeCompleto()]));
                return;
            }
            $model->estado='activo';
            $model->save();

            {
                $l = new LoginForm();
                $l->username = $model->nome;
                $l->password = $model->senha;
                $l->rememberMe = true;
                $l->login();
                $this->redirect("/");
            }
        }else{
            throw new NotFoundHttpException(yii::t('app','Pagina pedida não existe.'));
        }

    }

    public function actionSendmail($id){
        $model = $this->findModel($id);
        $this->sendmail($model);
    }

    private function sendmail($model){
        $mail = new Mailer();
        \Yii::$app->mail->compose('utilizador/confirmation', ['nome' => $model->getNomeCompleto(), 'email'=>$model->email, 'auth'=>$model->authenticationKey])
            ->setFrom([\Yii::$app->params['adminEmail'] => 'ParkOnline'])
            ->setSubject('Register Confirmation' )
            ->setTo($model->email)
            ->send();
    }

    /**
     * Updates an existing Utilizador model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Utilizador model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Utilizador model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Utilizador the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Utilizador::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    const MIN_SEARCH_LEN=0;
    public function actionAutocomplete(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $value    =Yii::$app->getRequest()->getQueryParam('value');
        $offset   =Yii::$app->getRequest()->getQueryParam('offset');
        $minLength =self::MIN_SEARCH_LEN;
        if($minLength>0 && ($value=='' || strlen($value)<$minLength)) {
            return [__METHOD__.' precisa pelo menos '.$minLength.' caracteres, para proceder'];
            return;
        }

        $ret=array();

        if( !empty($id) || is_numeric($value)) {
            $val=!empty($id) ? $id : $value;
            $model = Utilizador::find()
                ->select("id,nome,apelido,email")
                ->where( ["id"=> intval($val)] )->all();
        }
        if (!isset($model) && empty($id)) {
            $offset= $offset==''
                ? strlen($value)
                : (intval($offset)<$minLength
                    ? $minLength
                    : intval($offset));
            $val=substr($value,0,$offset);

            $model = Utilizador::find()
                ->select("id,nome,apelido,email")
                ->where( $val==''?'':['like',"concat(nome,' ',apelido)" ,"$val"] )
                ->all();
        }
        if ($model !== null) {
            foreach($model as $k=>$rec) {
                $ret[] = array(
                    'label'=>$rec['id'].' - '.$rec['nome'].' '.$rec['apelido'].' - '.$rec['email'],
                    'value'=>$rec['id'],
                );
            }
        }
        return $ret;
    }
}
