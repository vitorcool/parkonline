<?php

/* @var $this yii\web\View */
/* @var $get array */
/* @var $parque array */
/* @var $veiculos array */
/* @var $lugares array */
/* @var $reserva app\models\Reserva */


use yii\helpers\Html;

$this->title = $parque['nome'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mostraparque">

    <br>
    <image src= "https://cdn4.iconfinder.com/data/icons/banking-finance/32/walk-512.png" style= "width:50px; height:50px"><?= $get['distancia2'].', '.$get['tempo2'] ?>

    <image src= "https://d30y9cdsu7xlg0.cloudfront.net/png/72-200.png" style= "width:50px; height:50px"><?=$get['distancia1'].', '.$get['tempo1'] ?>
    <br>
    <br>
    <table align= "center" width= "400">


        <tr> <th colspan = "2"> Informações de ( <?=$parque['nome']?> )</th></tr>
        <tr>
            <th>Informação</th>
            <th>Resultado</th>
        </tr>

        <tr>
            <td>Data</td>
            <td><?=$reserva->data_reserva ?></td>
        </tr>
        <tr>
            <td>Preço por minuto</td>
            <td><?=$parque['preco_min'] ?>€</td>
        </tr>

        <tr>
            <td>Nº total de lugares</td>
            <td><?=$parque['total_lugares'] ?></td>
        </tr>


    </table>
    <br>
    <select id="tipo-lugar">
        <?php foreach($lugares as $lugar){?>
            <option value="<?=$lugar['id']?>"><?=$lugar['descricao']?></option>
        <?php } ?>
    </select>
    <br>
    <select id="veiculo">
        <?php foreach($veiculos as $veiculo){?>
            <option value="<?=$veiculo['id']?>"><?=$veiculo['marca'].' '.$veiculo['matricula']?></option>
        <?php } ?>
    </select>


    <!-- Reservar -->
    <button id="reservar" class="btn btn-primary ui-btn ui-shadow ui-corner-all" name="login-button">Reservar</button>


</div>

<script>
    $(document).ready(function(){
        $("button#reservar").click(function(e){
            var reserva_dat = {
                data_reserva:'<?=$reserva->data_reserva?>',
                id_veiculo:  $("select#veiculo").val(),
                id_parque:   <?=$reserva->id_parque?>,
                id_tipo_lugar:$("select#tipo-lugar").val(),
            };
            $.ajax({
                type: "GET",
                url: "/parque/reserva_pedido",
                data: reserva_dat ,
                dataType: "json",
                success:function(data,textStatus,jqXHR){
                    if(data['message']!='ok'){
                        if(typeof(data['message']) == 'string')
                            data['message']=[data['message']];
                        $.each(data['message'],function(i,mess){
                            window.alert( mess);
                        });
                    }else {
                        var url= '/parque/enviado_pedido/?id='+data.reserva.id;
                        //window.alert('redirect: '+url);
                        window.document.location.href=url;
                    }
                },
                error:function(jqXHR,textStatus,error){
                    window.alert(error);
                }

            });
        })
    })
</script>

