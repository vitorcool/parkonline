<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use app\assets\MapAsset;

MapAsset::register($this);

$this->title = 'Localizar estacionamento';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="localiza-destino">
    <h1><?= Html::encode($this->title) ?></h1>

    <div id="box_input">
        <form id="obter-rota" class="ui-filterable">
            <label for="origem">Origem</label>
            <input id="origem" class="findplaces">
            <label for="destino">Destino</label>
            <input id="destino" class="findplaces">
            <button type="submit" class="btn btn-primary ui-btn ui-shadow ui-corner-all" name="rota">Obter rota</button>
        </form>
    </div>
    <div id="box_separator"></div>
    <div id="map"></div>
</div>

<script>
    $(document).ready(function() {
        // active resize of $map between $box_input and footer:first

        $('ul#list-local li').click(function () {
            var texto = $(this).find('a').text();
            $('#destino').val(texto);
            $("#list-local").listview("refresh");

            $("#list-local li").attr('class', '');
            $("#list-local li").addClass('ui-screen-hidden');
        });

        $('#destino').blur(function () {
            $("#list-local li").attr('class', '');
            $("#list-local li").addClass('ui-screen-hidden');
        });


        $('label[for=origem]').dblclick(function(){
            getMyGeoLocation(function(lat,lng,address){
                window.saveMode=true;
                $('#origem').val(address);
                setMarker("origem",lat,lng,address);
            })
        })

    });

</script>