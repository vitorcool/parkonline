<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Meus locais';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div id="meus-locais">
        <img src="/img/emconstrucao.png" style="width: 98%" title="Em Construção" alt="Em Construção">
    </div>

</div>
