<?php

/* @var $this yii\web\View */
/* @var $reserva app\models\Reserva */

use yii\helpers\Html;

$this->title = Yii::t('app','Pedido de reserva');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">


    <br>
    <p>
        O seu pedido de reserva foi enviado ao parque seleccionado.
    </p>
    <p>
        A confirmação da reserva vais ser enviada por e-mail ou pode acompanhar o seu estado na página <a href="/reserva/view2/?id=<?=$reserva->id?>">estado de reserva</a>
    </p>

<?=$this->render('/reserva/reserva_view',['model'=>$reserva])?>

</div>
