<?php

/* @var $this yii\web\View */
/* @var $pendentes array*/
/* @var $confirmadas array*/
/* @var $dentro array*/


use yii\helpers\Html;

$this->title = Yii::t('app','Parque - Operações reservas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parque-operacoes">
    <h1><?= Html::encode($this->title) ?></h1>

    <h3>Pendentes</h3>
    <div id="pendentes" class="container-fluid">

        <?php foreach($pendentes as $res) { ?>
            <div class="col-xs-12 col-sm-6 col-lg-4 list-group-item" id="<?=$res['id']?>" data-id="<?=$res['id']?>">
                    <div class="row">
                        <div class="col-xs-12"><?=yii\helpers\BaseInflector::humanize($res['nome'].' '.$res['apelido'],true)  ?></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-4 title">NIF</div>
                        <div class="col-xs-8"><?=$res['idVeiculo']['idUtilizador']['nif']?></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 title">Veiculo</div>
                        <div class="col-xs-8"><?=$res['idVeiculo']['marca'].' '.$res['idVeiculo']['matricula']?></div>
                    </div>

                <div class="row">
                    <div class="col-xs-4 title">id/reserva_estado_id</div>
                    <div class="col-xs-8"><?=$res['id'].' '.$res['reserva_estado_id']?></div>
                </div>
                <div class="row">
                    <div class="col-xs-4 title">Data reserva</div>
                    <div class="col-xs-8"><?=$res['estado_reserva_data']?></div>
                </div>

                <form class="confirmar">
                        <div class="row">
                            <div class="col-xs-4 title">Código</div>
                            <div class="col-xs-8"><input type="text" name="codigo_acesso" id="codigo_acesso" value="<?=$res['codigo_acesso']?>"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 title">Lugar</div>
                            <div class="col-xs-8"><input type="text" name="lugar" id="lugar"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 title">Andar</div>
                            <div class="col-xs-8"><input type="text" name="andar" id="andar"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 title">Cor</div>
                            <div class="col-xs-8"><input type="text" name="cor" id="cor"></div>
                        </div>

                        <button id="confirmar" data-id="<?=$res['id']?>" class="btn btn-primary ui-btn ui-shadow ui-corner-all" name="login-button">Confirmar</button>

                </form>
                <div class="row">
                    <div class="col-xs-12">
                        <button id="rejeitar"  data-id="<?=$res['id']?>" class="btn btn-primary ui-btn ui-shadow ui-corner-all" name="login-button">Rejeitar</button>
                    </div>
                </div>

            </div>
        <?php } ?>
    </div>


    <h3>Confirmadas</h3>
    <div id="confirmadas" class="container-fluid">

        <?php foreach($confirmadas as $res) { ?>
            <div class="col-xs-12 col-sm-6 col-lg-4 list-group-item" id="<?=$res['id']?>" data-id="<?=$res['id']?>">
                    <div class="row">
                        <div class="col-xs-12"><?=yii\helpers\BaseInflector::humanize($res['idVeiculo']['idUtilizador']['nome'].' '.$res['idVeiculo']['idUtilizador']['apelido'],true)  ?></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 title">NIF</div>
                        <div class="col-xs-8"><?=$res['nif']?></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 title">Veiculo</div>
                        <div class="col-xs-8"><?=$res['matricula']?></div>
                    </div>

                <div class="row">
                    <div class="col-xs-4 title">id/reserva_estado_id</div>
                    <div class="col-xs-8"><?=$res['id'].' '.$res['reserva_estado_id']?></div>
                </div>
                <div class="row">
                    <div class="col-xs-4 title">Data confirmação</div>
                    <div class="col-xs-8"><?=$res['estado_reserva_data']?></div>
                </div>
                <div class="row">
                    <div class="col-xs-4 title">Código</div>
                    <div class="col-xs-8"><?=$res['codigo_acesso']?></div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button id="entrar" data-id="<?=$res['id']?>" class="btn btn-primary ui-btn ui-shadow ui-corner-all" name="login-button">Entrar</button>
                    </div>
                </div>

            </div>
        <?php } ?>
    </div>

    <h3>Dentro</h3>
    <div id="dentro" class="container-fluid">
        <?php foreach($dentro as $res) { ?>
            <div class="col-xs-12 col-sm-6 col-lg-4 list-group-item" id="<?=$res['id']?>" data-id="<?=$res['id']?>">
                    <div class="row">
                        <div class="col-xs-12"><?=yii\helpers\BaseInflector::humanize($res['idVeiculo']['idUtilizador']['nome'].' '.$res['idVeiculo']['idUtilizador']['apelido'],true)  ?></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-4 title">NIF</div>
                        <div class="col-xs-8"><?=$res['idVeiculo']['idUtilizador']['nif']?></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 title">Veiculo</div>
                        <div class="col-xs-8"><?=$res['idVeiculo']['marca'].' '.$res['idVeiculo']['matricula']?></div>
                    </div>
                <div class="row">
                    <div class="col-xs-4 title">id/reserva_estado_id</div>
                    <div class="col-xs-8"><?=$res['id'].' '.$res['reserva_estado_id']?></div>
                </div>
                <div class="row">
                    <div class="col-xs-4 title">Data entrada</div>
                    <div class="col-xs-8"><?=$res['estado_reserva_data']?></div>
                </div>
                <div class="row">
                    <div class="col-xs-4 title">Código</div>
                    <div class="col-xs-8"><?=$res['codigo_acesso']?></div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button id="sair" data-id="<?=$res['id']?>" class="btn btn-primary ui-btn ui-shadow ui-corner-all" name="login-button">Sair</button>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

</div>
<script>
    $(document).ready(function(){
        $("button#sair").click(function(e){
            var data= {
                id: $(e.target).data('id')
            };
            $.ajax({
                type: "GET",
                url: "/parque/reserva_sair",
                data: data,
                dataType: "json",
                success:function(data,textStatus,jqXHR){
                    if(data['message']!='ok'){
                        if(typeof(data['message']) == 'string')
                            data['message']=[data['message']];
                        $.each(data['message'],function(i,mess){
                            window.alert( mess);
                        });
                    }else {
                        window.document.location.reload();
                    }
                },
                error:function(jqXHR,textStatus,error){
                    window.alert(error);
                }
            });
        });
        $("button#entrar").click(function(e){
            var data= {
                id: $(e.target).data('id')
            };
            $.ajax({
                type: "GET",
                url: "/parque/reserva_entrar",
                data: data,
                dataType: "json",
                success:function(data,textStatus,jqXHR){
                    if(data['message']!='ok'){
                        if(typeof(data['message']) == 'string')
                            data['message']=[data['message']];
                        $.each(data['message'],function(i,mess){
                            window.alert( mess);
                        });
                    }else {
                        window.document.location.reload();
                    }
                },
                error:function(jqXHR,textStatus,error){
                    window.alert(error);
                }
            });
        });
        $("button#confirmar").click(function(e){
            var form = $(e.target).closest('form');
            var data= {
                id: $(e.target).data('id'),
                codigo_acesso : form.find('#codigo_acesso').val(),
                lugar: form.find('#lugar').val(),
                cor : form.find('#cor').val(),
                andar: form.find('#andar').val(),
            };

            $.ajax({
                type: "GET",
                url: "/parque/reserva_confirmar",
                data: data,
                dataType: "json",
                success:function(data,textStatus,jqXHR){
                    if(data['message']!='ok'){
                        if(typeof(data['message']) == 'string')
                            data['message']=[data['message']];
                        $.each(data['message'],function(i,mess){
                            window.alert( mess);
                        });
                    }else {
                        window.document.location.reload();
                    }
                },
                error:function(jqXHR,textStatus,error){
                    window.alert(error);
                }
            });
        });
        $("button#rejeitar").click(function(e){
            var data= {
                id: $(e.target).data('id')
            };
            $.ajax({
                type: "GET",
                url: "/parque/reserva_rejeitar",
                data: data,
                dataType: "json",
                success:function(data,textStatus,jqXHR){
                    if(data['message']!='ok'){
                        if(typeof(data['message']) == 'string')
                            data['message']=[data['message']];
                        $.each(data['message'],function(i,mess){
                            window.alert( mess);
                        });
                    }else {
                        window.document.location.reload();
                    }
                },
                error:function(jqXHR,textStatus,error){
                    window.alert(error);
                }
            });
        })


        var refreshPageTimer = null;
        var refreshInterval = 60; //segundos
        var startTime = (new Date()).getTime();
        startRefreshTimer = function(){
            window.setInterval(function(){
                var now=(new Date()).getTime();
                var dif_min = (now-startTime)/1000;
                if(dif_min > refreshInterval){
                    startTime = now;
                    window.document.location.reload();
                }
            },500);
        }
        startRefreshTimer();
    });
</script>