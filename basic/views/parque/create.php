<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Parque */

$this->title = Yii::t('app', 'Create Parque');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Parques'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parque-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
