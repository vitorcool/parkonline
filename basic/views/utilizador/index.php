<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UtilizadorSerch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Utilizadors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utilizador-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Utilizador'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],
            [
                'attribute'=>'#',
                'format'=>'raw',
                'value'=>function($model){
                    return $model->id;
                }
            ],
            [
                'attribute'=>'tipoUtilizador',
                'format'=>'raw',
                'value'=>function($model){
                    $a=['Administrador','Condutor','Parque'];
                    return $model->tipoUtilizador;
                }
            ],
            'estado',
            'nome',
            'apelido',

        //    'senha',
        //    'authenticationKey',
        //    'accessToken',
             'email:email',
             'telefone',
             'nif',
             'morada',
        ],
    ]); ?>
</div>
