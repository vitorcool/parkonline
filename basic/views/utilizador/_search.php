<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UtilizadorSerch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="utilizador-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nome') ?>


    <?php  echo $form->field($model, 'email') ?>

    <?php  echo $form->field($model, 'telefone') ?>

    <?php  echo $form->field($model, 'tipoUtilizador') ?>

    <?php  echo $form->field($model, 'nif') ?>

    <?php  echo $form->field($model, 'apelido') ?>

    <?php  echo $form->field($model, 'morada') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
