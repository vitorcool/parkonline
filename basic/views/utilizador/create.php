<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Utilizador */
/* @var $veiculo \app\models\Veiculo */

$this->title = Yii::t('app', 'Create Utilizador');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Utilizadors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utilizador-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'veiculo'=>$veiculo,
    ]) ?>

</div>
