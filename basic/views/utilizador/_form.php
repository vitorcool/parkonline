<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Utilizador */
/* @var $form yii\widgets\ActiveForm */
/* @var $veiculo \app\models\Veiculo */
?>

<div class="utilizador-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(!Yii::$app->user->isGuest && Yii::$app->user->getIdentity()->tipoUtilizador=="Admin"){
            echo $form->field($model, 'tipoUtilizador')->dropDownList(
                ['Admin' => 'Admin','Condutor' => 'Condutor','Parque' => 'Parque', ],
                array_merge(
                    ['prompt' => '',
                        'tipoUtilizador'=>$model->tipoUtilizador
                    ],
                    (Yii::$app->user->isGuest || Yii::$app->user->getIdentity()->tipoUtilizador!="Admin"?["disabled" => "disabled"]:[])
                )
            );
            echo $form->field($model, 'estado')->dropDownList(
                    ['pendente'=>'pendende','activo'=>'activo','desactivo'=>'desactivo'],
                    ['prompt' => '','estado'=>$model->estado]
            );

        echo $form->field($model, 'id_parque')->widget(\app\components\MyAutoComplete::classname(),
            [
                'ajaxUrl'       => '/parque/autocomplete' ,
                'options'       => ['class' => 'form-control',
                    //'readonly' => ! $model->isNewRecord,
                ],
                'clientOptions' => [
                    'name'  => 'Utilizador[id_parque]',
                    'value' => $model->id_parque,
                ]
            ]);
    }?>
    <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'apelido')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'senha')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'morada')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nif')->textInput(['maxlength' => true]) ?>

    <?php if(isset($veiculo) && Yii::$app->user->isGuest) { ?>
        <div class="panel panel-primary">
            <div class="panel-heading">Veiculo</div>
            <div class="panel-body">
                <?= $form->field($veiculo, 'matricula')->textInput(['maxlength' => true]) ?>

                <?= $form->field($veiculo, 'marca')->textInput(['maxlength' => true]) ?>

                <?= $form->field($veiculo, 'n_lugares')->textInput() ?>
            </div>
        </div>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
