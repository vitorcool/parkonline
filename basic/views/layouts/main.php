<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= "utf-8"//Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= \yii\helpers\Html::csrfMetaTags() ?>
    <title><?= \yii\helpers\Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="stylesheet" href="/css/parkonlinetest1.css" />
    <?php
    if(!empty(Yii::$app->controller->css)) :
        if(!is_array(Yii::$app->controller->css))
            Yii::$app->controller->css=[Yii::$app->controller->css];
            foreach(Yii::$app->controller->css as $cssFile) :
    ?>
                <link href='<?= $cssFile ?>' rel='stylesheet' type='text/css' />
    <?php
            endforeach;
        endif;
    ?>

    <?php
    if(!empty(Yii::$app->controller->javaScript)) :
        if(!is_array(Yii::$app->controller->javaScript))
            Yii::$app->controller->javaScript=[Yii::$app->controller->javaScript];
        foreach(Yii::$app->controller->javaScript as $javaScriptFile) :
            ?>
            <script type="text/javascript" src="<?= $javaScriptFile ?>"></script>
    <?php
        endforeach;
    endif;
    ?>

    <script>
        function initMap(){
            $(document).ready(function(){
                try{
                    initGMap()}
                catch(e){}
            })
        }
    </script>
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?v=3&language=pt&region=pt&callback=initMap&key=AIzaSyDUB6iRrkF2FGtTANln0okkSiNzA7jOFgg&libraries=places" async defer></script>

</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap clearfix">
    <?php
    \yii\bootstrap\NavBar::begin([
        'brandLabel' => Yii::t('app','ParkOnline'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if(! Yii::$app->user->isGuest){
        $esdadosItems=[];
        if( in_array(Yii::$app->user->getIdentity()->tipoUtilizador,['Admin','Condutor']) ){
            $estadosCount = \app\models\ReservaEstado::getCountReservas()->all();
            foreach($estadosCount as $idx=>$esdadoItem) {
                $esdadosItems[] = [
                    'label' =>  '<span class="submenu-estados">'     .$esdadoItem->descricao  . '</span>'.
                                '<span class="submenu-estados-ct"> ('.$esdadoItem->ct_reservas.')</span>',
                    'encode' => false,
                    'url' => '/reserva/estado/' . strtolower($esdadoItem->descricao)
                ];
            }
        }else if( in_array(Yii::$app->user->getIdentity()->tipoUtilizador,['Parque']) ){
            $estadosCount = \app\models\ReservaEstado::getCountReservasParque()->all();
            foreach($estadosCount as $idx=>$esdadoItem) {
                $esdadosItems[] = [
                    'label' =>  '<span class="submenu-estados">'     .$esdadoItem->descricao  . '</span>'.
                                '<span class="submenu-estados-ct"> ('.$esdadoItem->ct_reservas.')</span>',
                    'encode' => false,
                    'url' => '/reserva/estado/' . strtolower($esdadoItem->descricao)
                ];
            }
        }
        $nav=[
            [
                'label' =>  '<div class="menu-left-l1">'.(Yii::$app->user->getIdentity()->tipoUtilizador=='Admin'? 'Administrador':
                                                        Yii::$app->user->getIdentity()->tipoUtilizador) .'</div>'.
                            '<div class="menu-left-l2">'.yii::$app->user->getIdentity()->getNomeCompleto().'</div>',
                'url'   => '#',
                'encode'=> false,
                'items'=> array_merge(
                    (
                        Yii::$app->user->getIdentity()->tipoUtilizador!='Admin'?[]:
                        [
                                ['label' => 'Gestão Utilizadores', 'url' => '/utilizador'],
                                ['label' => 'Gestão Veiculos', 'url' => '/veiculo/admin'],
                                ['label' => 'Gestão Parques', 'url' => '/parque'],
                                ['label' => 'Gestão Reservas', 'url' => '/reserva'],
                                '<li class="divider"></li>',
                        ]
                    ),
                    (
                       in_array(Yii::$app->user->getIdentity()->tipoUtilizador,['Admin','Condutor'])?
                        [
                                ['label' => 'Perfil', 'url' => '/utilizador/update?id='.Yii::$app->user->id],
                                ['label' => 'Dados do automovel', 'url' => '/veiculo'],
                                ['label' => 'Meus locais', 'url' => '/site/meuslocais'],
                                ['label' => 'Minhas reservas', 'url' => '/reserva/estado/aprovado',
                                        'items'=> $esdadosItems
                                ],
                                  /*
                                            [
                                                ['label' => 'Pendente', 'url' => '/reserva/estado/pendente'],
                                                ['label' => 'Aprovado', 'url' => '/reserva/estado/aprovado'],
                                                ['label' => 'Rejeitado', 'url' => '/reserva/estado/rejeitado'],
                                                ['label' => 'Dentro', 'url' => '/reserva/estado/dentro'],
                                                ['label' => 'Fora', 'url' => '/reserva/estado/fora'],
                                                ['label' => 'Expirado', 'url' => '/reserva/estado/expirado']
                                            ]
                                  */

                                ['label' => 'Faturas', 'url' => '/site/faturas'],

                        ]:[]
                    ),
                    (
                        Yii::$app->user->getIdentity()->tipoUtilizador!='Parque'?[]:
                        [
                                ['label' => 'Operações', 'url' => '/parque/operacoes'],
                                ['label' => 'Reservas',
                                    'items'=> $esdadosItems
                                ],
                        ]
                    ),
                    (
                    [
                        '<li class="divider"></li>',
                        ['label' => 'Sair', 'url' => ['/site/logout']],
                    ]
                    )
                ),
            ],
            '<li class="divider"></li>',
            ['label' => Yii::t('app','Sobre'), 'url' => ['/site/about']],

        ];

        echo \yii\bootstrap\Nav::widget([
                            'options' => ['class' => 'navbar-nav navbar-left '],
                            'items' =>$nav
                        ]);
    }
    else
        \yii\bootstrap\Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => [
            ['label' => 'Entrar', 'url' => ['/site/login']],
            ['label' => Yii::t('app','Sobre'), 'url' => ['/site/about']],
        ],
    ]);
    \yii\bootstrap\NavBar::end();
    ?>

    <div class="container">
        <?= \yii\widgets\Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; ParkOnline <?= date('Y') ?></p>

        <?php //include "share.php" ?>
    </div>
</footer>

<?php $this->endBody() ?>

<script type="text/javascript">
    $("h1").fitText(1.2, { maxFontSize: '36px' });
    $('.breadcrumb .active').fitText(1.2, {maxFontSize: '36px' });
    (function () {
        $.mobile.ajaxEnabled = false;
        $.mobile.loadingMessage = false;


        // handle bug on yii menu for small screens
        $('button[data-toggle=collapse]').click(function(e) {
            var target = $(this).data('target');
            if( $(target).hasClass('in') ) {
                //$(target).removeClass('in',{duration:500});
                $(target).removeClass('in');
                return false;
            }else{
                $(target).addClass('in');
                $(target).find('li.dropdown').addClass('open');
                return false
            }
        });

/*        $('ul.navbar-nav li.dropdown').click(function(e){
            var o=$(this);
            o.toggleClass('open');
            e.preventDefault();
            return false;
        })*/

    }());

</script>

</body>
</html>
<?php $this->endPage() ?>
