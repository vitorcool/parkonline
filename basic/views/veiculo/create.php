<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\veiculo */
/* @var $asAdmin boolean */

$this->title = Yii::t('app', 'Criar Veiculo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Veiculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="veiculo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'asAdmin'=>isset($asAdmin) && $asAdmin,
    ]) ?>

</div>
