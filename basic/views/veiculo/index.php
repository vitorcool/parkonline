<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Veiculo2Search */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $asAdmin boolean */

if($asAdmin==true)
    $this->title = Yii::t('app', 'Gestão Veiculos');
else
    $this->title = Yii::t('app', 'Veiculos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="veiculo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Criar Veiculo'), ['create','admin'=>$asAdmin], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}{update}{delete}',
                'buttons'=>[
                    'update' => function ($url, $model) use ($asAdmin){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update','id'=>$model->id,'admin'=>$asAdmin], [
                            'title' => Yii::t('yii', 'Update'),
                        ]);

                    }
                ]
            ],
            [
                'attribute'=>'#',
                'format'=>'raw',
                'value'=>function($model){
                    return $model->id;
                }
            ],
            [
                'attribute'=>'condutor',
                'format'=>'raw',
                'value'=>function($model){
                    $utilizador=$model->idUtilizador;
                    if(! isset($utilizador))
                        return '';
                    else
                        return
                            '<p>'.$utilizador->getNomeCompleto().'</p>'.
                            '<p>NIF: '.$utilizador->nif.'</p>';
                },
                'visible'=>$asAdmin,
            ],
            'matricula',
            'marca',
            'n_lugares',
        ],
    ]); ?>

</div>
