<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\veiculo */
/* @var $form yii\widgets\ActiveForm */
/* @var $asAdmin boolean */

$asAdmin = isset($asAdmin) && $asAdmin==true && yii::$app->user->getIdentity()->tipoUtilizador=='Admin';
?>

<div class="veiculo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if($asAdmin==true ){?>
        <?php //echo $form->field($model, 'id_utilizador')->textInput() ?>
        <?= $form->field($model, 'id_utilizador')->widget(\app\components\MyAutoComplete::classname(),
            [
                'ajaxUrl'       => '/utilizador/autocomplete' ,
                'options'       => ['class' => 'form-control',
                    'readonly' => ! $model->isNewRecord,],
                'clientOptions' => [
                    'name'  => 'Veiculo[id_utilizador]',
                    'value' => $model->id_utilizador,
                ]
            ]) ?>
    <?php }else{ ?>
        <?= $form->field($model, 'id_utilizador')->hiddenInput()->label(false) ?>
    <?php }?>

    <?= $form->field($model, 'matricula')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'marca')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'n_lugares')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>