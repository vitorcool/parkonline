<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\veiculo */
/* @var $asAdmin boolean */

$this->title = Yii::t('app', 'Alterar veiculo', [
    'modelClass' => 'Veiculo',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Veiculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="veiculo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'asAdmin'=>isset($asAdmin) && $asAdmin,
    ]) ?>

</div>
