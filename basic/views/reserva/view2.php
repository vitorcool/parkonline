<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Reserva */

?>
<div class="reserva-view">
<?=  $this->render('reserva_view',['model'=>$model]); ?>
</div>
