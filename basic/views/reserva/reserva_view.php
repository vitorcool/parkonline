<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Reserva;

/* @var $this yii\web\View */
/* @var $model app\models\Reserva */

$this->title = 'Reserva: '.$model->id.' - ['.$model->reserva_estado_estado_desc.']';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reservas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?= Html::encode($this->title) ?></h1>

<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label'     => Yii::t('app','Estado'),
                'format'    =>'raw',
                'value'     => $model->reserva_estado_estado_desc
            ],
            [
                'label'     => Yii::t('app','Data'),
                'format'    =>'raw',
                'value'     => $model->estado_reserva_data
            ],
            [
                'label'     => Yii::t('app','Codigo acesso'),
                'format'    => 'raw',
                'value'     => '<img style="max-width: 300px" src = "http://barcode.tec-it.com/barcode.ashx?translate-esc=off&data='
                    .$model->codigo_acesso
                    .'&code=MobileQRCode&unit=Fit&dpi=96&imagetype=GIF&rotation=0&color=000000&bgcolor=ffffff&qunit=Mm&quiet=0&eclevel=L%27%20">'
                    .'<p>'.$model->codigo_acesso.'</p>',
                'visible'  => in_array($model->reserva_estado_estado_id,[Reserva::Reserva_Aprovado,Reserva::Reserva_Dentro,Reserva::Reserva_Fora])
            ],
            [
                'label'     => Yii::t('app','Veiculo'),
                'value'     => $model->idVeiculo->marca.' '.$model->idVeiculo->matricula
            ],
            [
                'label'     => Yii::t('app','Condutor'),
                'format'    => 'raw',
                'value'     => $model->idVeiculo->idUtilizador->getNomeCompleto()
                    .'<br>NIF: '.$model->idVeiculo->idUtilizador->nif,
            ],
            [
                'label'     => Yii::t('app','Parque').' - '
                    .$model->idParqueTipoLugar->idParque->nome,
                'format'    => 'raw',
                'value'     =>  '<p>'.$model->idParqueTipoLugar->idParque->morada.'</p>'.
                    '<p>'.$model->idParqueTipoLugar->idParque->cod_postal.' ' .$model->idParqueTipoLugar->idParque->localidade.'</p>'.
                    '<p>GPS: '.$model->idParqueTipoLugar->idParque->lat.', ' .$model->idParqueTipoLugar->idParque->lng.'</p>'.
                    '<p>'.$model->idParqueTipoLugar->idTipoLugar->descricao.'</p>'.
                    ''
            ],
            [
                'label'=>Yii::t('app','Hora Entrada'),
                'visible'=>in_array($model->reserva_estado_estado_id,[Reserva::Reserva_Dentro,Reserva::Reserva_Fora]),
                'value'=>$model->hora_entrada,
            ],
            [
                'label'=>Yii::t('app','Hora Saida'),
                'visible'=>in_array($model->reserva_estado_estado_id,[Reserva::Reserva_Fora]),
                'value'=>$model->hora_saida,
            ],
            [
                'label'=>Yii::t('app','Preço'),
                'visible'=>in_array($model->reserva_estado_estado_id,[Reserva::Reserva_Fora]),
                'value'=>$model->preco.'€',
            ],
            [
                'label'=>Yii::t('app','Lugar'),
                'visible'=>!empty($model->lugar),
                'value'=>$model->lugar,
            ],
            [
                'label'=>Yii::t('app','Cor'),
                'visible'=>!empty($model->cor),
                'value'=>$model->cor,
            ],
            [
                'label'=>Yii::t('app','Andar'),
                'visible'=>!empty($model->andar),
                'value'=>$model->andar,
            ],
        ],
    ])


?>
