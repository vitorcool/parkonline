<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Reserva;


/* @var $this yii\web\View */
/* @var $model app\models\Reserva */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reservas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reserva-view">
<?php if(yii::$app->user->getIdentity()->tipoUtilizador=='Admin') { ?>
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
<?php } ?>
    <?= $this->render('reserva_view',['model'=>$model]);
?>

</div>