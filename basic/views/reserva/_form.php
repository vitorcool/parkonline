<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Reserva */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reserva-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= //$form->field($model, 'id_veiculo')->textInput()
        $form->field($model, 'id_veiculo')->widget(\app\components\MyAutoComplete::classname(),
        [
            'ajaxUrl'       => '/veiculo/autocomplete' ,
            'options'       => ['class' => 'form-control',
                //'readonly' => ! $model->isNewRecord,
                                ],
            'clientOptions' => [
                'name'  => 'Reserva[id_veiculo]',
                'value' => $model->id_veiculo,
            ]
        ])
    ?>

    <?= $form->field($model, 'data_reserva')->textInput() ?>

    <?= //$form->field($model, 'id_parque')->textInput()
        $form->field($model, 'id_parque')->widget(\app\components\MyAutoComplete::classname(),
        [
            'ajaxUrl'       => '/parque/autocomplete' ,
            'options'       => ['class' => 'form-control',
                //'readonly' => ! $model->isNewRecord,
            ],
            'clientOptions' => [
                'name'  => 'Reserva[id_parque]',
                'value' => $model->id_parque,
            ]
        ])
    ?>

    <?= //$form->field($model, 'id_tipo_lugar')->textInput()
        $form->field($model, 'id_tipo_lugar')->widget(\app\components\MyAutoComplete::classname(),
        [
            'ajaxUrl'       => new \yii\web\JsExpression('function(){ return "/parque/autocomplete_tipolugar/"+$("input#reserva-id_parque").val()}') ,
            'options'       => ['class' => 'form-control',
                //'readonly' => ! $model->isNewRecord,
            ],
            'clientOptions' => [
                'name'  => 'Reserva[id_tipo_lugar]',
                'value' => $model->id_tipo_lugar,
            ]
        ])
    ?>

    <?= $form->field($model, 'codigo_acesso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hora_entrada')->textInput() ?>

    <?= $form->field($model, 'hora_saida')->textInput() ?>

    <?= $form->field($model, 'preco')->textInput() ?>

    <?= $form->field($model, 'lugar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'andar')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
