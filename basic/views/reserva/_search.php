<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReservaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reserva-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_veiculo') ?>

    <?= $form->field($model, 'data_reserva') ?>

    <?= $form->field($model, 'id_parque') ?>

    <?= $form->field($model, 'id_tipo_lugar') ?>

    <?php // echo $form->field($model, 'codigo_acesso') ?>

    <?php // echo $form->field($model, 'hora_entrada') ?>

    <?php // echo $form->field($model, 'hora_saida') ?>

    <?php // echo $form->field($model, 'preco') ?>

    <?php // echo $form->field($model, 'lugar') ?>

    <?php // echo $form->field($model, 'cor') ?>

    <?php // echo $form->field($model, 'andar') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
