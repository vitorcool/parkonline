<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Reserva;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReservaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\Reserva */

$this->title = isset($title) ? $title : Yii::t('app', 'Reservas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reserva-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
<?php if(false) {?>
    <p>
        <?= Html::a(Yii::t('app', 'Create Reserva'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php }?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} '.
                ( yii::$app->user->getIdentity()->tipoUtilizador=='Admin'?'{update} {delete}':'')],
            [
                'attribute'=>'#',
                'format'=>'raw',
                'value'=>function($model){
                    return $model->id;
                }
            ],
            [
                'attribute'=>'condutor',
                'format'=>'raw',
                'value'=>function($model){
                    return
                            '<p>'.$model->idVeiculo->idUtilizador->getNomeCompleto().'</p>'.
                            '<p>NIF: '.$model->idVeiculo->idUtilizador->nif.'</p>'.
                            '<p>'.$model->idVeiculo->marca.' '.$model->idVeiculo->matricula.'</p>';
                },
                'visible' => yii::$app->user->getIdentity()->tipoUtilizador!='Condutor',
            ],
            [
                'attribute'=>'veiculo',
                'format'=>'raw',
                'value'=>function($model){
                    return
                        '<p>'.$model->idVeiculo->marca.' '.$model->idVeiculo->matricula.'</p>';
                },
                'visible' => yii::$app->user->getIdentity()->tipoUtilizador=='Condutor',
            ],
            [
                'attribute' => 'parque',
                'format'=>'raw',

                'value'=>function($model){
                    return
                        '<p>'.$model->idParqueTipoLugar->idParque->nome.'</p>'.
                        '<p>'.$model->idParqueTipoLugar->idParque->morada.'</p>'.
                        '<p>'.$model->idParqueTipoLugar->idParque->cod_postal.' ' .$model->idParqueTipoLugar->idParque->localidade.'</p>'.
                        '<p>GPS: '.$model->idParqueTipoLugar->idParque->lat.', ' .$model->idParqueTipoLugar->idParque->lng.'</p>'.
                        '<p>'.$model->idParqueTipoLugar->idTipoLugar->descricao.'</p>'.
                        '';
                },
                'visible' => yii::$app->user->getIdentity()->tipoUtilizador!='Parque',
            ],
            [
                'attribute'=>'estado',
                'format'=>'raw',
                'value'=>function($model){

                    $u = $model->getUltimoEstado();
                    $ret='<p>['. $u->reserva_estado_estado_desc . ']</p>';
                    if($u->reserva_estado_estado_id.''==Reserva::Reserva_Pendente.'') {
                        $ret .= '<div style="font-size: small;">' .
                            'Data pedido: ' . $u->estado_reserva_data . '</div>';
                        $ret .= '<div style="font-size: small;">' .
                            'Tempo desde pedido: ' . Reserva::diffString(date('Y-m-d H:i:s'),$u->estado_reserva_data) . '</div>';
                    }
                    if($u->reserva_estado_estado_id.''==Reserva::Reserva_Aprovado.'') {
                        $ret .= '<div style="font-size: small;">' .
                            'Data aprovação: ' . $u->estado_reserva_data . '</div>';
                        $pendente = $model->findEstado(Reserva::Reserva_Pendente);
                        $ret .= '<div style="font-size: small;">' .
                            'Tempo de aprovação: ' . Reserva::diffString($u->estado_reserva_data,$pendente->dia_hora) . '</div>';
                    }
                    if($u->reserva_estado_estado_id.''==Reserva::Reserva_Dentro.'') {
                        $ret .= '<div style="font-size: small;">' .
                            'Data entrada: ' . $u->estado_reserva_data . '</div>';
                        $ret .= '<div style="font-size: small;">' .
                            'Tempo desde entrada: ' . Reserva::diffString(date('Y-m-d H:i:s'),$u->estado_reserva_data) . '</div>';
                    }
                    if($u->reserva_estado_estado_id.''==Reserva::Reserva_Fora .'') {

                        $model->updateReservaSaida();

                        $ret .= '<div style="font-size: small;">' .
                            'Data saida: ' . $u->estado_reserva_data . '</div>';
                        $entrada = $model->findEstado(Reserva::Reserva_Dentro);
                        $ret .= '<div style="font-size: small;">' .
                            'Tempo de estacionamento: ' . Reserva::diffString($entrada->dia_hora,$u->estado_reserva_data) . '</div>';
                        $ret .= '<div style="font-size: small;">' .
                            'Preço: ' . $model->preco . '€ </div>';
                    }

                    return $ret;
                },
            ]

        ],
    ]); ?>

</div>
