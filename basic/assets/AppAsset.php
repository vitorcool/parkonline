<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
        "https://code.jquery.com/mobile/1.4.5/jquery.mobile.structure-1.4.5.min.css",
        '/css/jquery.mobile.icons.min.css',
        '/css/site.css',
        'https://fonts.googleapis.com/css?family=Convergence|Bitter|Droid+Sans|Ubuntu+Mono',
    ];
    public $js = [
        "https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",
        "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js",
        "https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js",
        "/js/jquery.fittext.js",
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
//        'yii\web\YiiAsset',
 //       'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
