<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'About' => '',
    'Access Token' => '',
    'Access denied. The action "{action}" needs authentication.' => '',
    'Apelido' => '',
    'Are you sure you want to delete this item?' => '',
    'Authentication Key' => '',
    'Can not delete file.' => '',
    'Can not delete order {order} from route ID {id}' => '',
    'Can not delete route ID {id}' => '',
    'Create' => '',
    'Create Rota' => '',
    'Create Utilizador' => '',
    'Criar Utilizador' => '',
    'Delete' => '',
    'Descricao' => '',
    'Email' => '',
    'Entrar' => '',
    'Ficheiro' => '',
    'Historia' => '',
    'ID' => '',
    'Icon' => '',
    'Invalid data' => '',
    'Lat' => '',
    'Lembra de mim' => '',
    'Link Imagem' => '',
    'Link Miniatura' => '',
    'Link Remover' => '',
    'Lng' => '',
    'Method Not Allowed' => '',
    'Morada' => '',
    'Nif' => '',
    'Nome' => '',
    'Order' => '',
    'ParkOnline' => '',
    'Please contact us if you think this is a server error. Thank you.' => '',
    'Please fill out the following fields to login:' => '',
    'Point order {order} from route ID {id} is missing' => '',
    'Ponto ID' => '',
    'Procura' => '',
    'Reset' => '',
    'Rota ID' => '',
    'Rotas' => '',
    'Route ID is missing' => '',
    'Route data is missing' => '',
    'Sair' => '',
    'Search' => '',
    'Senha' => '',
    'Sobre' => '',
    'Tamanho' => '',
    'Telefone' => '',
    'The above error occurred while the Web server was processing your request.' => '',
    'Tipo Remoção' => 'Tipo Remoção',
    'Tipo Utilizador' => '',
    'Update' => '',
    'Update {modelClass}: ' => '',
    'Updating route id is missing' => '',
    'Utilizador' => '',
    'Utilizador ID' => '',
    'Utilizador ou senha incorrecta.' => '',
    'Utilizadors' => '',
    'Criar novo Utilizador' => '@@@@',
    'Código de Verificação' => '@@@@',
    'Ponto' => '@@@@',
];
