<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=192.168.33.10;dbname=parkonline',
    'username' => 'root',
    'password' => 'qwe123',
    'charset' => 'utf8',
    'on afterOpen' => function($event) {
        date_default_timezone_set ( "Europe/Lisbon" );
        $event->sender->createCommand("SET time_zone = '+00:00'")->execute();
    },
];
