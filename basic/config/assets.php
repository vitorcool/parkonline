<?php

return [
    'bundles' => [
        'yii\web\JqueryAsset' => [
            'sourcePath' => null,   // do not publish the bundle
            'js' => [],
            'css'=> [],
            'jsOptions'=>['position'=>1],
        ],
        'yii\bootstrap\BootstrapAsset' => [
            'sourcePath' => null,
            'js' => [],
            'css' =>[],
            'jsOptions'=>['position'=>1],
        ],
        'yii\bootstrap\BootstrapPluginAsset' => [
            'sourcePath' => null,
            'js' => [],
        ]
    ],
];
