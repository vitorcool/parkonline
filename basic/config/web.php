<?php

$params = require(__DIR__ . '/params.php');
$db     = require(__DIR__ . '/db.php');
$assets = require(__DIR__ . '/assets.php');
$mail   = require(__DIR__ . '/mail.php');
$config = [
    'id' => 'basic',
    'language' => 'pt',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'on beforeRequest' => function ($event) {
        if(!Yii::$app->request->isSecureConnection){
            // add some filter/exemptions if needed ..
            $url = Yii::$app->request->getAbsoluteUrl();
            $url = str_replace('http:', 'https://', $url);
            Yii::$app->getResponse()->redirect($url);
            Yii::$app->end();
        }
    },
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Yave5dG4YZGUFBEMP3ZGTPJW8U6XO_O4',
            'class' => '\yii\web\Request',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'enableCookieValidation' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            //'identityClass' => 'app\models\User',
            //'enableAutoLogin' => true,
            'identityClass' => 'app\models\User',
            'enableSession' => true,
            'loginUrl' => null,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'mail'=>$mail,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'reserva/estado/<estado:\w+>'=>'reserva/estado',
                'parque/autocomplete_tipolugar/<id_parque:\d+>'=>'parque/autocomplete_tipolugar',
            ],
        ],
        'assetManager' => $assets,
    ],

    'params' => $params,
];

if (YII_ENV_DEV)
{
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' =>array($_SERVER['REMOTE_ADDR']),//['127.0.0.1', '::1','192.168.1.*'], // adjust this to your needs

    ];
}

return $config;
