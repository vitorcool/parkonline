<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= "utf-8"//Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/angular.js"></script>

    <script type="text/javascript" src="/js/gmaps.js"></script>


    <link href='//fonts.googleapis.com/css?family=Convergence|Bitter|Droid+Sans|Ubuntu+Mono' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="css/parkonlinetest1.css" />
    <link rel="stylesheet" href="css/jquery.mobile.icons.min.css" />
    <link rel="stylesheet" href="//code.jquery.com/mobile/1.4.5/jquery.mobile.structure-1.4.5.min.css" />
    <?php
    if(!empty(Yii::$app->controller->css)) :
        if(!is_array(Yii::$app->controller->css))
            Yii::$app->controller->css=[Yii::$app->controller->css];
            foreach(Yii::$app->controller->css as $cssFile) :
    ?>
                <link href='<?= $cssFile ?>' rel='stylesheet' type='text/css' />
    <?php
            endforeach;
        endif;
    ?>

    <?php
    if(!empty(Yii::$app->controller->javaScript)) :
        if(!is_array(Yii::$app->controller->javaScript))
            Yii::$app->controller->javaScript=[Yii::$app->controller->javaScript];
        foreach(Yii::$app->controller->javaScript as $javaScriptFile) :
            ?>
            <script type="text/javascript" src="<?= $javaScriptFile ?>"></script>
    <?php
        endforeach;
    endif;
    ?>
    <link href='/css/site.css' rel='stylesheet' type='text/css' />
    <link href='/css/styles.css' rel='stylesheet' type='text/css' />
    <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?libraries=places&language=pt&region=pt&callback=initMap" async defer></script>

    <script  type="text/javascript">
        var tryModule = function(name) {

            // attempt to load the module into m
            var m;
            try {
                m = angular.module(name)
            } catch(err) {
                m = angular.module(name,[]);
            }

            // if it could not be loaded, try the rest of
            // the options. if it was, return it.
            return m;
        };

    </script>

</head>
<body ng-app="lauoutApp">
<?php $this->beginBody() ?>
<div class="wrap clearfix">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::t('app','ParkOnline'),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if(! Yii::$app->user->isGuest)
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-left ',
                          'ng-controller'=>'rotasController'],
    //        'items' => [
    //            ['label' => 'Nova rota', 'url' => '/rotas/nova'],
     //           [
     //               'label' => 'Minhas Rotas',
     //               'url'   => '#',
     //               'items' => [
                        //'<li class="divider"></li>',
     //                   '<li data-ng-repeat="myroute in rotas">
     //                       <a ng-click="loadroute(myroute)" href="#">{{myroute.descricao}}</a> </li>',
      //              ],
//                ],


  //          ]
        ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('app','Sobre'), 'url' => ['/site/about']],
            //['label' => Yii::t('app','Contacto'), 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Entrar', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    Yii::t('app','Sair').' (' . Yii::$app->user->identity->nome . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; ParkOnline <?= date('Y') ?></p>

        <?php //include "share.php" ?>
    </div>
</footer>

<?php yii\web\View::clear() ?>
<?php $this->endBody() ?>
<script type="text/javascript">
    (function () {
        // handle bug on yii menu for small screens
        //<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse" aria-expanded="true"><span class="sr-only">Toggle
        $('button[data-toggle=collapse]').click(function(e) {
            var target = $(this).data('target');
            if( $(target).hasClass('in') ) {
                $(target).removeClass('in',{duration:500});
                return false;
            }
        });


        tryModule('routeApp').controller('rotasController',
            ['$rootScope','$scope','$http','$location',
                function($rootScope,$scope,$http,$location){

                    $scope.load=function( callback ) {
                        $http.get("/rota/list").
                            success(function (data) {
                                if (data.message != 'ok') {
                                    if (typeof(data.message) == 'string')
                                        data.message = [data.message];
                                    jQuery.each(data.message, function (i, mess) {
                                        Lobibox.notify('error', {
                                            msg: mess,
                                            sound: false,
                                            delay: 2000,
                                            size: 'mini'
                                        });
                                    });
                                } else {
                                    if (typeof(callback) == "function")
                                        callback(data);
                                    $scope.rotas=data.data;
                                }
                            }).error(function (jqXHR, textStatus, error) {
                                Lobibox.notify('error', {
                                    msg: error,
                                    sound: false,
                                    delay: 2000,
                                    size: 'mini'
                                });
                            });
                    }

                    $rootScope.$on('loadRotas',function(){
                        $scope.load();
                    })

                    $scope.loadroute=function(rota){
                        window.location.href = '/rotas/altera?id='+rota.id;
                    }

                    $scope.rotas=[];
                    $rootScope.$emit('loadRotas');
                }]);


    }());

</script>

</body>
</html>
<?php $this->endPage() ?>
