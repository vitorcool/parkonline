/**
 * Created by vao on 07/04/2016.
 */

(function () {
    'use strict';

    var url='/upload/';


    var routeApp=angular.module('routeApp',[]);


    routeApp.controller('routeController',
        ['$rootScope','$scope','$http','$location','$timeout','$sce',
            function($rootScope,$scope,$http,$location,$timeout,$sce) {
                var route = function(){
                    return window.parkMap;
                }

                var RotaData=new function() {
                    var me=this;
                    // return route id
                    this.id = function () {
                        if(typeof(this.getData())=="undefined")
                            return parseInt('x');
                        else
                            return this.getData().data.Rota.id;
                    }
                    // return route information
                    this.getData = function () {
                        return $scope.rota;
                    };

                    $scope.pontoAddress=function(ponto){
                        return (typeof(ponto.address)=="undefined")
                            ? "Lat: "+ponto.lat+" Lng: "+ponto.lng
                            : ponto.address;
                    }

                    $scope.showRotaDescricao=function(){
                        $rootScope.$emit('novaRotaFormShow');
                    }

                    this.mergeAddressList=function(data,addressList) {

                        $(data.data.Pontos).each(function(i,ponto){
                            if(i<addressList.length) {
                                data.data.Pontos[i].address = addressList[i];
                            }
                        });
                        return data;
                    }

                    // load route information from server and set data
                    this.load = function (id, callback) {
                        $http.get("/rota/get?id=" + id).
                            success(function (data) {
                                if (data.message != 'ok') {
                                    if (typeof(data.message) == 'string')
                                        data.message = [data.message];
                                    jQuery.each(data.message, function (i, mess) {
                                        Lobibox.notify('error', {
                                            msg: mess,
                                            sound: false,
                                            delay: 2000,
                                            size: 'mini'
                                        });
                                    });
                                } else {
                                    if (typeof(callback) == "function")
                                        callback(data);
                                    else
                                        me.setData(data);
                                }
                            }).error(function (jqXHR, textStatus, error) {
                                Lobibox.notify('error', {
                                    msg: error,
                                    sound: false,
                                    delay: 2000,
                                    size: 'mini'
                                });
                            });
                    }


                    this.setData = function (data) {
                        var ret=[];
                        angular.forEach(data.data.Pontos,function(ponto){
                            angular.forEach(ponto.Fotos,function(foto){
                                ret.push($.extend(foto,ponto,{foto_id:foto.id}));
                            })
                        })
                        $scope.$apply(function(){
                            $scope.rota = data;
                            $scope.pontos = data.data.Pontos;
                            $rootScope.$emit('updateSlides',ret);
                            //$scope.allSlides=ret;
                        });
                    }

                    $scope.$watchCollection('rota', function (newVal, oldVal) {
                        //if(typeof(newVal)!='undefined'){}
                    });
                }

                $scope.sceHtml = function(val){
                    return $sce.trustAsHtml(val);
                }

                var getGeoLocation=function(callback) {
                    function returnPosition(position) {
                        if(typeof(callback)=="function")
                            callback({lat:position.coords.latitude, lng:position.coords.longitude});
                    }
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(returnPosition);
                    } else if(typeof(callback)=="function"){
                        callback({lat: 39.391714,lng: -9.130700});  // default geo location
                    }
                }


                var loadRoute=function(updateMap) {
                    updateMap = typeof(updateMap)=="undefined"?true:updateMap;
                    var id=isNaN(RotaData.id()) ? urlParam('id') : RotaData.id();
                    RotaData.load(id, function (data) {
                        if (route() != null && updateMap)
                            route().applyServerData(data);
                        if (route() != null) {
                            if (route().map.markers.length > 0) {
                                route().drawRoute(function (addressList) {
                                    data = RotaData.mergeAddressList(data, addressList);
                                    RotaData.setData(data);
                                    $timeout(function(){
                                        route().updateInfoWindow();
                                    },500);
                                });
                            } else {
                                getGeoLocation(function (position) {
                                    route().map.setCenter(position.lat, position.lng);
                                    RotaData.setData(data);
                                });
                            }
                        }
                    });
                }

                $scope.inc=function(a){
                    return parseInt(a)+1;
                }

                function urlParam(name) {
                    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                    if (results == null) {
                        return null;
                    }
                    else {
                        return results[1] || 0;
                    }
                }

                // start controller
                $scope.rota = {data:{ Rota  :{id:parseInt('x'),descricao:"Nova Rota..."},
                    Pontos:[] }};
                $scope.pontos = [];


                $timeout(function(){
                    var id= urlParam('id');
                    loadRoute();
                },500);

    }]);

    routeApp.controller('markController',
        ['$rootScope','$scope','$http','$location','$timeout','$sce',
            function($rootScope,$scope,$http,$location,$timeout,$sce) {
        $scope.allSlides = [];
        console.log($scope.allSlides);
        $scope.$watch('allSlides', function(newval,oldval) {
            console.log(newval, oldval);
        });

        $rootScope.$on('updateSlides', function (event,data) {
            $timeout(function(){
                $scope.$apply(function(){
                    $scope.allSlides= data;
                })
            },100);
        });
    }]);


    $(document).on('click','#map ul.slides li',function(e){
        var pontoOrder=$(e.currentTarget).data('ponto');
        var foto=$(e.currentTarget).data('foto');
        var indicators = $(".modal-transparent .carousel-indicators");
        var slideOrder= parseInt(foto);
        var indicator=null;
        if(slideOrder==0){
            indicator = indicators.find('li[data-ponto='+pontoOrder+']:first');
            if(indicator.length==0) return;
        }else
            indicator = indicators.find('li[data-foto='+foto+']:first');
        var fotoIndex = indicator.data("foto");

        $("#allFotosWindow").modal();
        var fotoSlides=$("#allFotosWindow .carousel-inner .item");
        var fotoSlide=fotoSlides.filter("[data-foto="+fotoIndex+"]");
        fotoSlides.removeClass("active");
        indicators.removeClass("active");
        fotoSlide.addClass("active");
        indicator.addClass("active");
        //indicator.trigger('click');

    })

    $(document).on('shown.bs.modal', ".modal-transparent",function () {
        setTimeout( function() {
            $(".modal-backdrop").addClass("modal-backdrop-transparent");
            $(window).trigger('resize');
        }, 0);
    });

    $(document).on('hidden.bs.modal', ".modal-transparent", function () {
        $(".modal-backdrop").addClass("modal-backdrop-transparent");
    });

    $(document).on('shown.bs.modal', ".modal-fullscreen",function () {
        setTimeout( function() {
            $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
            $(window).trigger('resize');
        }, 0);
    });

    $(document).on('hidden.bs.modal', ".modal-fullscreen", function () {
        $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
    });



}());



var rotaScope = function(){
    return angular.element($("[ng-controller=routeController]")[0]).scope();
}



