/**
 * Created by vao on 23/03/2016.
 */

$(document).ready(function(){
    var Leftpanel= function( panel ){
        var me=this;
        var panelA_minSize=4;
        var panelA_extendedMaxSize=500;
        var navHeight = $('nav:first').length>0
                ? $('nav:first').height()
                  + parseInt($('nav:first').css('padding-top'))
                  + parseInt($('nav:first').css('margin-top'))
                  + 3
                :0;
        var footerHeight = $('footer:first').length>0
                ? $('footer:first').height()
                + parseInt($('footer:first').css('padding-top'))
                + parseInt($('footer:first').css('margin-top'))
                + 3
                :0;
        this.objA = $(panel);




        this.calcWidth=function(){
            var availableWidth = $(window).width();
            var panelWidth = availableWidth-panelA_minSize;
            panelWidth = Math.min(panelA_extendedMaxSize,panelWidth);
            return panelWidth;
        }

        this.calcHeight=function(){
            var availableHeight = $(window).height();
            var panelHeight = availableHeight-navHeight-footerHeight;
            return panelHeight;
        }

        me.objA.css({
            position:'absolute',
            width:me.calcWidth(),
            left:panelA_minSize-me.calcWidth(),
            top:navHeight,
            minHeight:me.calcHeight(),
            maxHeight:me.calcHeight(),
            height:me.calcHeight(),
            display:'',
            padding:10
        });

        this.show = function() {
            me.objA.stop(true).animate({left: 0}, function (e) {
                me.objA.addClass("extended-panel");
            });
            //this.objB.stop(true).animate({left: panelA_minSize});
        }

        this.hide = function() {
            this.objA.stop(true).animate({left:panelA_minSize-me.calcWidth()}, function (e) {
                me.objA.removeClass("extended-panel");
            });
            //this.objB.stop(true).animate({left: panelA_extendedSize});
        }

        this.objA.mouseenter(function(e){
            me.show();
        });

        /*this.objA.mouseout(function(e){
            var overObject= $(e.relatedTarget);
            if(!overObject.closest(me.objA).length>0)
                me.hide();
        });*/
        this.objA.find('button[type=button][class=close]').click(function(e){
            me.hide();
        });

        $(window).resize(function() {
            me.objA.css({
                width:me.calcWidth(),
                left:panelA_minSize-me.calcWidth(),
                top:navHeight,
                minHeight:me.calcHeight(),
                maxHeight:me.calcHeight(),
                height:me.calcHeight(),
            });
            me.objA.removeClass("extended-panel");

            me.objA.find('container-fluid').css('maxHeight',me.calcHeight()-131);
        });
    }
    window.leftPanel= new Leftpanel('#left-autohide');

});
