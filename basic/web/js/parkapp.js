/**
 * Created by vao on 07/04/2016.
 */


(function () {
    'use strict';

    var routeApp=angular.module('parkOnlineApp')
        .config([
            '$httpProvider',
            function ($httpProvider) {
                delete $httpProvider.defaults.headers.common['X-Requested-With'];
            }
        ]);


    routeApp.controller('parkController', [
        '$rootScope','$scope','$element', '$http', '$filter', '$window',
        function ($rootScope,$scope, $element,$http) {

        }
    ]);

}());

var parkScope = function(){
    return angular.element($("[ng-controller=parkController]")[0]).scope();
}
