/**
 * Created by vitor on 30/05/2017.
 */

$(document).ready(function () {
    // resize google map between $box_separator and footer:first
    window.resizeMap = new ResizeMap('#map', '#box_separator', 'footer:first');
    window.setTimeout(function(){
        initGMap();
    },1500);
})

Storage.prototype.setObj = function(key, obj) {
    return this.setItem(key, JSON.stringify(obj))
};

Storage.prototype.getObj = function(key) {
    return JSON.parse(this.getItem(key))
};

$(document).ready(function () {
    // START - handle form submit
    $("form#obter-rota").submit(function(e){
        e.preventDefault();
        window.setTimeout(function() {
                drawRoute(function (e) {
                    map.fitZoom();
                })
            }
            ,500);
        return false;
    });
    // END - handle form submit
})

// START - show & hide box_input
var box_input_visible=$('#box_input').is(':visible');
var box_input_click_handler=function(){
    if( box_input_visible+'' =='false' ) {
        $('#box_input').css('position', 'absolute');
        //   $('#box_input').css('opacity', '.7');
        $('#box_input').show();
    }
}

$(document).ready(function () {
    var box_input_mouseleave_handler=function(){
        if( box_input_visible+'' =='false' ) {
            $('#box_input').css('position', 'initial');
            //     $('#box_input').css('opacity', '1');
            $('#box_input').hide();
        }
    }
    $("h1").click(function(){
        toggle_box_input();
    });
    //.mouseenter(box_input_click_handler);

    //$("#box_input").mouseleave(box_input_mouseleave_handler);
})

hide_box_input=function(callback){
    $('#box_input').hide({
        duration: "slow",
        done: function (e) {
            box_input_visible = $('#box_input').is(':visible');
            $(window).trigger('resize');
            map.refresh();
            if (typeof(callback) == "function")
                callback(e);
        },
        progress: function () {
            $(window).trigger('resize');
            map.refresh();
        }
    });
}
show_box_input=function(callback){
    $('#box_input').show({
        duration: "slow",
        done: function (e) {
            box_input_visible = $('#box_input').is(':visible');
            $(window).trigger('resize');
            map.refresh();
            if (typeof(callback) == "function")
                callback(e);
        },
        progress: function () {
            $(window).trigger('resize');
            map.refresh();
        }
    });
}
toggle_box_input=function(callback){
    if(box_input_visible+''=='true')
        hide_box_input(callback)
    else
        show_box_input(callback)
}
// END - show & hide box_input



var parks=null;
var saveMode=true;

var addParks=function(arrParks){

    var addPark=function(park){
        var p=function(st){
            return '<p>'+st+'</p>';
        }
        var marker = map.addMarker({
            lat: park.lat,
            lng: park.lng,
            Title: park.nome,
            click: function(marker) {
                map.setCenter(marker.getPosition().lat(),marker.getPosition().lng());
                map.setZoom(16);
                for(f=2;f<map.markers.length;f++){
                    map.markers[f].setIcon('https://maps.google.com/mapfiles/ms/icons/purple-dot.png');
                }
                marker.setIcon('https://maps.google.com/mapfiles/ms/icons/red-dot.png');

                drawRoute2(marker,function(){},function(){
                    var idx = map.markers.indexOf(marker)-2;
                    var url= '/site/mostraparque/?id='+parks[idx].id+
                        '&origem='+$("#origem").val()+
                        '&tempo1='+tempo1+
                        '&distancia1='+distancia1+
                        '&destino='+$('#destino').val()+
                        '&tempo2='+tempo2+
                        '&distancia2='+distancia2;
                    window.document.location.href=url;
                });
            },
            infoWindow: {
                content: p(park.nome)+p(park.morada)+p(park.cod_postal+' '+park.localidade)+
                p('Preco minuto: '+park.preco_min)+p('GPS: '+park.lat+', '+park.lng)
            },
            icon:'https://maps.google.com/mapfiles/ms/icons/purple-dot.png'
        });
        return marker;
    }

    parks=arrParks;
    for(f=0;f<parks.length;f++){
        addPark(parks[f]);
    }

    if(window.saveMode==true)
        window.sessionStorage.setObj('parks',parks);
    //drawRoute();
}

var loadOnlineParks = function(){
    map_bounds= {
        lat: [map.getBounds().f.f, map.getBounds().f.b],
        lng: [map.getBounds().b.f, map.getBounds().b.b]
    };

    $.ajax({
        type: "GET",
        url: "/site/onlineparks",
        data: map_bounds ,
        dataType: "json",
        success:function(data,textStatus,jqXHR){
            if(data['message']!='ok'){
                if(typeof(data['message']) == 'string')
                    data['message']=[data['message']];
                $.each(data['message'],function(i,mess){
                    window.alert( mess);
                });
            }else {
                while(map.markers.length>2) {
                    map.removeMarker(map.markers[map.markers.length-1]);
                }

                addParks(data.parks);
            }
        },
        error:function(jqXHR,textStatus,error){
            window.alert(error);
        }

    });
}

setMarker = function(input,lat,lng,title,autoFitRoute){
    autoFitRoute = typeof(autoFitRoute)=="undefined"?true:autoFitRoute;

    var updateMarker=function(){
        var marker= input=='origem'? map.markers[0] : map.markers[1];
        marker.setTitle(input+":"+title);
        marker.setPosition( new google.maps.LatLng( lat, lng ) );
        return marker;
    }

    var addMarker=function() {

        var marker = map.addMarker({
            lat: lat,
            lng: lng,
            Title: input+":"+title,
            draggable: true,
            dragend: function (e) {
                var marker = this;
                if (map.markers.indexOf(marker)==0) {//origem
                    getReverseGeocodingData(marker.getPosition().lat(),marker.getPosition().lng() ,
                        function(address){
                            $('#origem').val(address);
                            marker.setTitle("origem:"+address);
                            if(window.saveMode==true)
                                window.sessionStorage.setObj("origem",{address:address,
                                    lat:marker.getPosition().lat(),
                                    lng:marker.getPosition().lng() });
                        }
                    );
                }else{
                    getReverseGeocodingData(marker.getPosition().lat(),marker.getPosition().lng() ,
                        function(address){
                            $('#destino').val(address);
                            marker.setTitle("destino:"+address);
                            if(window.saveMode==true)
                                window.sessionStorage.setObj("destino",{address:address,
                                    lat:marker.getPosition().lat(),
                                    lng:marker.getPosition().lng() });
                        }
                    );
                }
                drawRoute();
            },
            click: function(marker) {
                map.setCenter(marker.getPosition().lat(),marker.getPosition().lng());
                map.setZoom(15);
                loadOnlineParks();
            }
        });
        return marker;
    }

    if((input=="origem" && map.markers.length==0) ||
        (input=="destino" && map.markers.length<2))
        marker=addMarker();
    else
        marker=updateMarker();

    if(window.saveMode==true)
        window.sessionStorage.setObj(input,{address:title,lat:lat,lng:lng});

    if (input=="origem")
        marker.setIcon('https://maps.google.com/mapfiles/ms/icons/yellow-dot.png');
    else
        marker.setIcon('https://maps.google.com/mapfiles/ms/icons/green-dot.png');

    if(autoFitRoute==true)
        fitRoute();
        
        
    drawRoute(function(){
        if(input=="destino")
            hide_box_input(function(){
                //if(autoFitRoute==true)
                //    fitRoute();
            });

    })

    return marker;

}

fitRoute = function () {
    if(map.markers.length>1)
        map.fitZoom();
    else if(map.markers.length>0){
        var e = map.markers[0].position;
        map.setCenter(e.lat(), e.lng());
    }

}


var tempo1='';
var tempo2='';
var distancia1='';
var distancia2='';
drawRoute = function (callback) {
    if (map.markers.length<2 )return;
    var origin = map.markers[0].getPosition();
    var destination = map.markers[1].getPosition();
    var i = 0, waypoints = [];


    map.cleanRoute();
    map.drawRoute({
        //origin: origin,
        //destination: destination,
        origin: [origin.lat(), origin.lng()],
        destination: [destination.lat(), destination.lng()],
        waypoints: waypoints,
        optimizeWaypoints: false,//waypoints.length != 0,
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
        strokeColor: '#131540',
        strokeOpacity: 0.6,
        strokeWeight: 6,
        callback: function (e, status) {
            tempo1 = e.legs[0].duration.text;
            distancia1 = e.legs[0].distance.text;
            if (typeof(callback) == "function")
                callback(e);
        }
    });

};

drawRoute2 = function(marker,callback1,callback2){
    if (map.markers.length<2 )return;
    if (map.markers.length==2 ) drawRoute(callback1);

    var origin = map.markers[0].getPosition();
    var destination = marker.getPosition();
    var i = 0, waypoints = [];


    map.cleanRoute();
    map.drawRoute({
        //origin: origin,
        //destination: destination,
        origin: [origin.lat(), origin.lng()],
        destination: [destination.lat(), destination.lng()],
        waypoints: waypoints,
        optimizeWaypoints: false,//waypoints.length != 0,
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
        strokeColor: '#131540',
        strokeOpacity: 0.6,
        strokeWeight: 6,
        unitSystem: google.maps.UnitSystem.METRIC,
        callback: function (e,status) {
            tempo1= e.legs[0].duration.text;
            distancia1= e.legs[0].distance.text;
            if (typeof(callback1) == "function")
                callback1(e);
        }
    });

    var origin = marker.getPosition();
    var destination = map.markers[1].getPosition();
    var i = 0, waypoints = [];

    map.drawRoute({
        //origin: origin,
        //destination: destination,
        origin: [origin.lat(), origin.lng()],
        destination: [destination.lat(), destination.lng()],
        waypoints: waypoints,
        optimizeWaypoints: false,//waypoints.length != 0,
        travelMode: google.maps.DirectionsTravelMode.WALKING,
        strokeColor: '#809fff',
        strokeOpacity: 0.6,
        strokeWeight: 6,
        unitSystem: google.maps.UnitSystem.METRIC,
        callback: function (e,status) {
            tempo2= e.legs[0].duration.text;
            distancia2= e.legs[0].distance.text;
            if (typeof(callback2) == "function")
                callback2(e);
        }
    });
}


//***************************************
gMapsinitiated=false;
window.initGMap=function() {
    if(gMapsinitiated==true) return;
    gMapsinitiated = true;
    window.map = new GMaps({
        div: $("#map")[0],
        lat: 39.391714,
        lng: -9.130700,
        zoom: 16,
        styles: [{
            "elementType": "geometry",
            "stylers": [{"hue": "#ff4400"}, {"saturation": -68}, {"lightness": -4}, {"gamma": 0.72}]
        }, {"featureType": "road", "elementType": "labels.icon"}, {
            "featureType": "landscape.man_made",
            "elementType": "geometry",
            "stylers": [{"hue": "#0077ff"}, {"gamma": 3.1}]
        }, {
            "featureType": "water",
            "stylers": [{"hue": "#00ccff"}, {"gamma": 0.44}, {"saturation": -33}]
        }, {
            "featureType": "poi.park",
            "stylers": [{"hue": "#44ff00"}, {"saturation": -23}]
        }, {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [{"hue": "#007fff"}, {"gamma": 0.77}, {"saturation": 65}, {"lightness": 99}]
        }, {
            "featureType": "water",
            "elementType": "labels.text.stroke",
            "stylers": [{"gamma": 0.11}, {"weight": 5.6}, {"saturation": 99}, {"hue": "#0091ff"}, {"lightness": -86}]
        }, {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [{"lightness": -48}, {"hue": "#ff5e00"}, {"gamma": 1.2}, {"saturation": -23}]
        }, {
            "featureType": "transit",
            "elementType": "labels.text.stroke",
            "stylers": [{"saturation": -64}, {"hue": "#ff9100"}, {"lightness": 16}, {"gamma": 0.47}, {"weight": 2.7}]
        }],
        click: function (e) {
            google.maps.event.trigger(window.map.map, 'rightclick',e);
        },
        zoom_changed: function (e) {
            //   $("#zoomLevel").val( me.zoom() );
            //   $("#ZoomLabel").html('Zoom: '+me.zoom() );
        },
        idle:function(e){
            //   $("#zoomLevel").val( me.zoom() );
            //   $("#ZoomLabel").html('Zoom: '+me.zoom() );
        }

    });

    try {
        autocomplete0 = new google.maps.places.Autocomplete($('input.findplaces')[0], {types: ['geocode']});
        autocomplete1 = new google.maps.places.Autocomplete($('input.findplaces')[1], {types: ['geocode']});
    }catch(e){

    }

    google.maps.event.addListener(map.map, "bounds_changed", function(){
        if(window.saveMode==true){
                var center = window.map.getCenter();
                var zoom = window.map.getZoom();
                window.sessionStorage.setObj('map', {center:center,zoom:zoom});
		};

    });

    autocomplete0.addListener('place_changed', function() {
        var place = autocomplete0.getPlace();

        setMarker("origem",place.geometry.location.lat(),place.geometry.location.lng(),$('input#origem').val());
    });

    autocomplete1.addListener('place_changed', function() {
        var place = autocomplete1.getPlace();

        setMarker("destino",place.geometry.location.lat(),place.geometry.location.lng(),$('input#destino').val());

    });

    //restore session
    var origem  =window.sessionStorage.getObj('origem');
    var destino =window.sessionStorage.getObj('destino');
    var arrParks=window.sessionStorage.getObj('parks');
    var bmap    =window.sessionStorage.getObj('map');
    window.saveMode=false;
    if(origem==null)
        getMyGeoLocation(function(lat,lng,address){
            window.saveMode=true;
            $('#origem').val(address);
            setMarker("origem",lat,lng,address);
        })
    else{
        $('#origem').val(origem.address);
        setMarker("origem",origem.lat,origem.lng,origem.address, false);
    }
    if(destino!=null) {
        $('#destino').val(destino.address);
        setMarker("destino",destino.lat,destino.lng,destino.address,false);
    }
    if(arrParks!=null) {
        addParks(arrParks);
    }
    window.setTimeout(function(){
        if(bmap!=null) {        
            window.map.map.setCenter(bmap.center);
            window.map.map.setZoom(bmap.zoom);        
        }
        window.saveMode=true;
    },1500);
};

window.getMyGeoLocation=function(callback) {
    function returnPosition(position) {
        getReverseGeocodingData(position.coords.latitude, position.coords.longitude ,
            function(address){
                if(typeof(callback)=="function")
                    callback(position.coords.latitude, position.coords.longitude,address);
            }
        );
    }
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(returnPosition);
    } else if(typeof(callback)=="function"){
        return null;
    }
}

function getReverseGeocodingData(lat, lng, callback) {
    var latlng = new google.maps.LatLng(lat, lng);
    // This is making the Geocode request
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status !== google.maps.GeocoderStatus.OK) {
            alert(status);
        }
        // This is checking to see if the Geoeode Status is OK before proceeding
        if (status == google.maps.GeocoderStatus.OK) {
            if (typeof(callback) == "function") {
                callback(results[0].formatted_address);
            }

        }
    });
}