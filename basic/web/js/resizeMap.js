/**
 * Created by vao on 23/03/2016.
 */


var ResizeMap= function( mapObj, topObj, bottomObj ){
    var me=this;
    var nav=$(topObj);
    //var nav=$('nav:first');
    var footer=$(bottomObj);
    //var footer=$('footer:first');
    this.navHeight = function(){
        return nav.length>0
            ? nav.position().top
            + nav.height()
            + parseInt(nav.css('padding-top'))
            + parseInt(nav.css('padding-bottom'))
            + parseInt(nav.css('margin-top'))
            + parseInt(nav.css('margin-bottom'))
            + 3
            :0;
    }
    this.footerHeight = function(){
        return footer.length>0
            ? footer.height()
            + parseInt(footer.css('padding-top'))
            + parseInt(footer.css('margin-top'))
            + 3
            :0;
    }

    this.mapObj = $(mapObj);

    this.calcWidth=function(){
        var availableWidth = $(window).width();
        return availableWidth;
    }

    this.calcHeight=function(){
        var availableHeight = $(window).height()-me.navHeight()-me.footerHeight();
        return availableHeight;
    }

    me.mapObj.css({
        position:'absolute',
        width:me.calcWidth(),
        left:0,
        top:me.navHeight(),
        height:me.calcHeight(),
        display:'',
    });

    $(window).resize(function() {
        me.mapObj.css({
            width:me.calcWidth(),
            left:0,
            top:me.navHeight(),
            minHeight:me.calcHeight(),
        });
    });
}

/*$(document).ready(function(){
    //window.resizeMap= new ResizeMap('#map');

});*/
