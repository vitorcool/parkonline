/**
 * Created by vao on 07/04/2016.
 */


(function () {
    'use strict';

    CKEDITOR.on( 'instanceCreated', function( event ) {
        var editor = event.editor,
            element = editor.element;

        // Customize editors for headers and tag list.
        // These editors don't need features like smileys, templates, iframes etc.
        if ( element.is( 'h1', 'h2', 'h3' ) || element.getAttribute( 'id' ) == 'taglist' ) {
            // Customize the editor configurations on "configLoaded" event,
            // which is fired after the configuration file loading and
            // execution. This makes it possible to change the
            // configurations before the editor initialization takes place.
            editor.on( 'configLoaded', function() {

                // Remove unnecessary plugins to make the editor simpler.
                editor.config.removePlugins = 'colorbutton,find,flash,font,' +
                    'forms,iframe,image,newpage,removeformat,' +
                    'smiley,specialchar,stylescombo,templates';

                // Rearrange the layout of the toolbar.
                editor.config.toolbarGroups = [
                    { name: 'editing',		groups: [ 'basicstyles', 'links' ] },
                    { name: 'undo' },
                    { name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
                    { name: 'about' }
                ];
            });
        }
    });

    var url='/upload/';
    var routeApp=angular.module('routeApp',['blueimp.fileupload','ngCkeditor'])
        .config([
            '$httpProvider', 'fileUploadProvider',
            function ($httpProvider, fileUploadProvider) {
                delete $httpProvider.defaults.headers.common['X-Requested-With'];
                fileUploadProvider.defaults.redirect = window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                );
                {
                    angular.extend(fileUploadProvider.defaults, {
                        // Enable image resizing, except for Android and Opera,
                        // which actually support image resizing, but fail to
                        // send Blob objects via XHR requests:
                        disableImageResize: /Android(?!.*Chrome)|Opera/
                            .test(window.navigator.userAgent),
                        maxFileSize: 999000,
                        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
                    });
                }
            }
        ]);

    routeApp.controller('jumperController', ['$scope','$window',
        function($scope,$window){
            $scope.jumpTo=function(order){
                function goToByScroll( order ){
                    var elem = '.ponto_historia[data-order='+order+']';
                    var elem0 = '.ponto_historia[data-order=0]';
                    // Scroll
                    $("#left-autohide .container-fluid").first().animate({
                            scrollTop: $(elem).offset().top-$(elem0).offset().top},
                        'slow');
                }
                goToByScroll(order);
            }
        }]);

    routeApp.controller('historiaController', ['$rootScope','$scope', '$element','$log', '$http', '$timeout',
        function($rootScope,$scope,$element, $log, $http, $timeout) {
            $scope.editorOptions = {
                language: 'pt'
                // uiColor: '#000000'
            };

            // handle save historia
            var myTimeOut=null;
            var lastSave=$scope.ponto.historia;
            $scope.save = function(event,hist) {
                if(typeof(event)=='undefined') return;
                var hist=$(event.target).html()
                var send2Server=function(){
                    var sendData = {
                        id:$scope.ponto.id,
                        historia: hist
                    };
                    if(lastSave!=hist)
                        jQuery.ajax({
                            url: '/rota/updatehistoria',
                            data: sendData,
                            method: 'POST',
                            dataType: "json",
                            success:function(data,textStatus,jqXHR){
                                if(data['message']!='ok'){
                                    if(typeof(data['message']) == 'string')
                                        data['message']=[data['message']];
                                    $.each(data['message'],function(i,mess){
                                        Lobibox.notify('error', {
                                            msg: mess,
                                            sound:false,
                                            delay:2000,
                                            size:'mini'
                                        });
                                    });
                                }else {
                                    lastSave=sendData.historia;
                                }
                            },
                            error:function(jqXHR,textStatus,error){
                                Lobibox.notify('error', {
                                    msg: error,
                                    sound:false,
                                    delay:2000,
                                    size:'mini'
                                });
                            }

                        });
                }

                if (myTimeOut != null)
                    $timeout.cancel(myTimeOut);
                myTimeOut = $timeout(function () {
                    send2Server();
                    myTimeOut = null;
                }, 500);
            }

        }]);

    routeApp.directive('ckeditor', [function () {
        var options = {
            toolbar: 'full',
            toolbar_full: [ //jshint ignore:line
                {
                    name: 'basicstyles',
                    items: ['Bold', 'Italic', 'Strike', 'Underline']
                },
                {name: 'paragraph', items: ['BulletedList', 'NumberedList', 'Blockquote']},
                {name: 'editing', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'tools', items: ['SpellChecker', 'Maximize']},
                '/',
                {
                    name: 'styles',
                    items: ['Format', 'Font','FontSize', 'TextColor', 'PasteText', 'PasteFromWord', 'RemoveFormat']
                },

                '/',
                {name: 'insert', items: [ 'Smiley','Table', 'SpecialChar']},
                {name: 'forms', items: ['Outdent', 'Indent']},
                {name: 'clipboard', items: ['Undo', 'Redo']},
                {name: 'document', items: ['PageBreak', 'Source']}
            ],

            disableNativeSpellChecker: false,
            colorButton_enableAutomatic :true,
            uiColor: '#FAFAFA',
            height: '400px',
            width: '100%'
        };
        return {
            restrict: 'A', // only activate on element attribute
            scope: false,
            require: 'ngModel',
            controller: function($scope, $element, $attrs) {}, //open for now
            link: function($scope, element, attr, ngModel, ngModelCtrl) {
                if(!ngModel) return; // do nothing if no ng-model you might want to remove this
                element.bind('click', function(){
                    for(name in CKEDITOR.instances)
                        CKEDITOR.instances[name].destroy();
                    CKEDITOR.inline(element[0],options);
                });
            }
        }
    }])

    routeApp.controller('MapMarkerFileUploadController', [
        '$rootScope','$scope','$element', '$http', '$filter', '$window',
        function ($rootScope,$scope, $element,$http) {
            $scope.options = {
                url: url
            };

            $scope.loadingFiles=true;
            $scope.queue=[];

            var order=$scope.ponto.order;
            $scope.loadFotos=function(fotos)
            {
                $scope.queue = fotos;
                $scope.loadingFiles=false;

            }
            $scope.loadFotos($scope.ponto.Fotos);

            $rootScope.$on('loadFotos',function(event,ponto){
                order = ponto.order;
                $scope.loadFotos(ponto.Fotos);
            });

            $scope.$on('fileuploadalways',function(e,data){
                // save file data
                var fotos=[];
                $(data.files).each(function(i,file){
                    fotos.push({
                        ponto_id: e.currentScope.ponto.id,
                        ficheiro: file.name,
                        historia:''
                    });
                });


                jQuery.ajax({
                    url: '/rota/insertfotos',
                    method: 'POST',
                    dataType: "json",
                    data: {fotos:fotos},
                    success:function(data,textStatus,jqXHR){
                        if(data['message']!='ok'){
                            if(typeof(data['message']) == 'string')
                                data['message']=[data['message']];
                            $.each(data['message'],function(i,mess){
                                Lobibox.notify('error', {
                                    msg: mess,
                                    sound:false,
                                    delay:2000,
                                    size:'mini'
                                });
                            });
                        }else {

                        }
                    },
                    error:function(jqXHR,textStatus,error){
                        Lobibox.notify('error', {
                            msg: error,
                            sound:false,
                            delay:2000,
                            size:'mini'
                        });
                    }

                });

                return;
            })
        }
    ]);

    routeApp.controller('FileDestroyController', [
        '$scope', '$http','$element',
        function ($scope, $http,$element) {
            var file = $scope.file,
                state;
            if (file.url) {
                file.$state = function () {
                    return state;
                };
                file.$destroy = function () {
                    state = 'pending';
                    jQuery.ajax({
                        url: '/rota/deletefoto',
                        method: 'POST',
                        dataType: "json",
                        data: {ficheiro: file.name, ponto_id: $scope.ponto.id},
                        success: function (data, textStatus, jqXHR) {
                            if (data['message'] != 'ok') {
                                if (typeof(data['message']) == 'string')
                                    data['message'] = [data['message']];
                                $.each(data['message'], function (i, mess) {
                                    Lobibox.notify('error', {
                                        msg: mess,
                                        sound: false,
                                        delay: 2000,
                                        size: 'mini'
                                    });
                                });
                                state = 'rejected';
                            } else {
                                $scope.clear(file);
                                state = 'resolved';
                            }
                        },
                        error: function (jqXHR, textStatus, error) {
                            Lobibox.notify('error', {
                                msg: error,
                                sound: false,
                                delay: 2000,
                                size: 'mini'
                            });
                        }

                    })
                }
            } else if (!file.$cancel && !file._index) {
                file.$cancel = function () {
                    $scope.clear(file);
                };
            }
        }
    ]);

    routeApp.controller('novaRotaController',[
        '$rootScope','$scope','$http','$timeout',
        function($rootScope,$scope,$http,$timeout){
            var route = function(){
                return window.parkMap;
            }
            var form = function(){
                return $("#novaRotaForm");
            }


            $scope.show = function(){
                function checkValidHidden() {
                    $rootScope.$emit('checkValidHidden');
                }
                // setup nova rota
                form().modal();
                form().on("hidden", checkValidHidden);
                form().on('hidden.bs.modal',checkValidHidden);
                $timeout(function(){
                    $('input#nomeRota')[0].select();
                },500);
            }

            $scope.hide=function(){
                $scope.rotaDescricaoError='';
                form()  .modal('hide');
            }

            $rootScope.$on('novaRotaFormShow',function(){
                $scope.show();
            })

            $rootScope.$on('novaRotaFormhide',function(event,args){
                $scope.hide();
            })

            $scope.cancelForm = function(){
                window.location.href='/';
            }

            $scope.rotaDescricaoError='';
            $scope.submitForm = function() {
                $scope.rotaDescricaoError="";
                jQuery.ajax({
                    url: '/rota/addupdate',
                    method: 'POST',
                    dataType: "json",
                    data: {'descricao':$scope.rota.data.Rota.descricao,
                                    id:$scope.rota.data.Rota.id},
                    success:function(data,textStatus,jqXHR){
                        if(data['message']!='ok'){
                            if(typeof(data['message']) == 'string')
                                data['message']=[data['message']];
                            $scope.rotaDescricaoError=data.message.descricao[0];
                            $rootScope.$emit('loadRotas');
                        }else {
                            if(window.location.pathname!='/rotas/altera')
                                window.location.href='/rotas/altera?id='+data['data']['Rota']['id'];
                            $scope.hide();
                        }
                    },
                    error:function(jqXHR,textStatus,error){
                        $scope.rotaDescricaoError=error;
                    }

                });
            }
        }]);


    routeApp.controller('routeController',
      ['$rootScope','$scope','$http','$location','$timeout',
      function($rootScope,$scope,$http,$location,$timeout) {
        var route = function(){
            return window.parkMap;
        }
        var RotaData=new function() {
            var me=this;
            // return route id
            this.id = function () {
                if(typeof(this.getData())=="undefined")
                    return parseInt('x');
                else
                    return this.getData().data.Rota.id;
            }
            // return route information
            this.getData = function () {
                return $scope.rota;
            };

            $scope.pontoAddress=function(ponto){
                return (typeof(ponto.address)=="undefined")
                    ? "Lat: "+ponto.lat+" Lng: "+ponto.lng
                    : ponto.address;
            }

            $scope.showRotaDescricao=function(){
                $rootScope.$emit('novaRotaFormShow');
            }

            this.mergeAddressList=function(data,addressList) {

                $(data.data.Pontos).each(function(i,ponto){
                    if(i<addressList.length) {
                        data.data.Pontos[i].address = addressList[i];
                    }
                });
                return data;
            }

            // load route information from server and set data
            this.load = function (id, callback) {
                $http.get("/rota/get?id=" + id).
                    success(function (data) {
                        if (data['message'] != 'ok') {
                            if (typeof(data['message']) == 'string')
                                data['message'] = [data['message']];
                            $.each(data['message'], function (i, mess) {
                                Lobibox.notify('error', {
                                    msg: mess,
                                    sound: false,
                                    delay: 2000,
                                    size: 'mini'
                                });
                            });
                        } else {
                            me.setData(data,function(){
                                if (typeof(callback) == "function")
                                    callback(data);
                            });
                            //else

                        }
                    }).error(function (jqXHR, textStatus, error) {
                        Lobibox.notify('error', {
                            msg: error,
                            sound: false,
                            delay: 2000,
                            size: 'mini'
                        });
                    });
            }


            this.setData = function (data,callback) {
                $timeout(function(){
                    $scope.$apply(function(){
                        $scope.rota = data;
                        $scope.pontos = data.data.Pontos;
                    });
                    if(typeof(callback)=="function")
                        callback();
                },50);
            }

            $scope.$watchCollection('rota', function (newVal, oldVal) {
                //if(typeof(newVal)!='undefined'){}
            });
        }

        var getGeoLocation=function(callback) {
            function returnPosition(position) {
                if(typeof(callback)=="function")
                    callback({lat:position.coords.latitude, lng:position.coords.longitude});
            }
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(returnPosition);
            } else if(typeof(callback)=="function"){
                callback({lat: 39.391714,lng: -9.130700});  // default geo location
            }
        }


        var loadRoute=function(updateMap) {
            updateMap = typeof(updateMap)=="undefined"?true:updateMap;
            var id=isNaN(RotaData.id()) ? urlParam('id') : RotaData.id();
            RotaData.load(id, function (data) {
                if (route() != null && updateMap)
                    route().applyServerData(data);
                if (route() != null) {
                    if (route().map.markers.length > 0) {
                        route().drawRoute(function (addressList) {
                            data = RotaData.mergeAddressList(data, addressList);
                            RotaData.setData(data);
                        });
                    } else {
                        getGeoLocation(function (position) {
                            route().map.setCenter(position.lat, position.lng);
                            RotaData.setData(data);
                        });
                    }
                    $rootScope.$emit('loadRotas');
                }
            });
        }

        $scope.inc=function(a){
            return parseInt(a)+1;
        }

        function urlParam(name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            }
            else {
                return results[1] || 0;
            }
        }

        $rootScope.$on('novaRotaFormShow',function(){
            return;
        })

        $scope.rota = {data:{ Rota  :{id:parseInt('x'),descricao:"Nova Rota..."},
              Pontos:[] }};
        $scope.pontos = [];
       $timeout(function(){
            var id= urlParam('id');
            function lancaFormNovaRota() {
                if (isNaN(id) || id==null) {
                    $rootScope.$emit('novaRotaFormShow');
                }else{
                    loadRoute();
                }
            }
            $rootScope.$on('checkValidHidden',lancaFormNovaRota);
            $scope.$on('reload',function() {
                loadRoute(false);
            });

            lancaFormNovaRota();
        },500);



    }]);

}());

var rotaScope = function(){
    return angular.element($("[ng-controller=routeController]")[0]).scope();
}
